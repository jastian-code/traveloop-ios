//
//  AirportPickerViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/12/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit


class AirportPickerViewController: UITableViewController, AirportPickable, UISearchBarDelegate, UISearchDisplayDelegate {
    
    @IBAction func closeButton(sender: AnyObject) {
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    var airports : [Airport] = []
    var countriesFromAirport: [Airport] = []
    
    var filteredAirport: [Airport] = []
    var filteredCountriesFromAirport: [Airport] = []
    
    var airportPickerTitle : String?{
        didSet{
            languageDict = languageDict.count == 0 ? GlobalFunction.getLanguageDict() : languageDict
            self.title = "\(languageDict[airportPickerTitle!]!)"
        }
    }
    var airportPickerSelectedValue : String?
    var airportPickerSelectedValueCity : String?
    var airportPickerSelectedText: String?
    
    var languageDict : [String: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageDict = languageDict.count == 0 ? GlobalFunction.getLanguageDict() : languageDict
        
        let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        airports = Airport.getAll(context) ?? [Airport]()
        
        var unique:[String:Bool] = [:]
        countriesFromAirport = self.airports.filter{
            unique.updateValue(false, forKey: String($0.countryId) as String) ?? true
        }
        
        filteredAirport = airports
        var unique2:[String:Bool] = [:]
        filteredCountriesFromAirport = self.filteredAirport.filter{
            unique2.updateValue(false, forKey: String($0.countryId) as String) ?? true
        }
        
    }
    
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var countryId : NSNumber?
        
        var listAirport = [Airport]()
        
        if(tableView == self.searchDisplayController!.searchResultsTableView){
            countryId = self.filteredCountriesFromAirport[section].countryId
            listAirport = self.filteredAirport.filter{ $0.countryId == countryId }
        }else{
            countryId = self.countriesFromAirport[section].countryId
            listAirport = airports.filter{ $0.countryId == countryId }
        }
        
        return listAirport.count
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if(tableView == self.searchDisplayController!.searchResultsTableView){
            return self.filteredCountriesFromAirport.count;        }
        else{
            return self.countriesFromAirport.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = self.tableView.dequeueReusableCellWithIdentifier("cellAirport", forIndexPath: indexPath) as? UITableViewCell
        
        
        let countryId:NSNumber?
        
        
        var listAirport = [Airport]()
        if tableView == self.searchDisplayController!.searchResultsTableView{
            countryId = self.filteredCountriesFromAirport[indexPath.section].countryId
            listAirport = self.filteredAirport.filter{ $0.countryId == countryId }
            
        }else{
            countryId = self.countriesFromAirport[indexPath.section].countryId
            listAirport = self.airports.filter{ $0.countryId == countryId }
            
        }
        
        let lblCity = cell!.viewWithTag(1) as? UILabel
        lblCity?.text = listAirport[indexPath.row].cityCountryOption
        
        let lblAirport = cell!.viewWithTag(2) as? UILabel
        lblAirport?.text = listAirport[indexPath.row].nameCodeOption
        
        let lblHiddenValue = cell!.viewWithTag(3) as? UILabel
        lblHiddenValue?.text = String(listAirport[indexPath.row].airportCode!)
        lblHiddenValue?.hidden = true

        let lblHiddenValueText = cell!.viewWithTag(4) as? UILabel
        lblHiddenValueText?.text = listAirport[indexPath.row].codeCityOption
        lblHiddenValueText?.hidden = true
        
        let lblHiddenValueCity = cell!.viewWithTag(5) as? UILabel
        lblHiddenValueCity?.text = listAirport[indexPath.row].city!.cityName!
        lblHiddenValueCity?.hidden = true
        
        return cell!
        
        
    }
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if tableView == self.searchDisplayController!.searchResultsTableView{
            return self.filteredCountriesFromAirport[section].countryName
        }else{
            return self.countriesFromAirport[section].countryName
        }
        
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        
    }
    
    var isEdit = false
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        isEdit = true
    }
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        isEdit = false
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let airportPickerSelectedCell = (sender as? UITableViewCell){
            airportPickerSelectedValue = (airportPickerSelectedCell.viewWithTag(3) as? UILabel)?.text;
            airportPickerSelectedText = (airportPickerSelectedCell.viewWithTag(4) as? UILabel)?.text;
            
            if var implementAirportPickerViewController = segue.destinationViewController as? AirportPickable{
                implementAirportPickerViewController.airportPickerSelectedValue = airportPickerSelectedValue
                implementAirportPickerViewController.airportPickerSelectedValueCity = airportPickerSelectedValueCity
                implementAirportPickerViewController.airportPickerSelectedText = airportPickerSelectedText
                implementAirportPickerViewController.airportPickerTitle = airportPickerTitle
                
            }
        }
    }
    
    
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchString searchString: String?) -> Bool {
        self.filterContentForSearchText(searchString!)
        self.tableView.reloadData()
        
        return true
    }
    
    func searchDisplayController(controller: UISearchDisplayController, shouldReloadTableForSearchScope searchOption: Int) -> Bool {
        self.filterContentForSearchText(self.searchBar.text!)
        self.tableView.reloadData()
        return true
    }
    
    func filterContentForSearchText(searchText: String){
        self.filteredAirport = self.airports.filter({ (airport : Airport) -> Bool in
            let stringMatch = airport.nameCodeCityCountryOption.rangeOfString(searchText, options: NSStringCompareOptions.CaseInsensitiveSearch)
            return stringMatch != nil
        })
        
        var unique:[String:Bool] = [:]
        self.filteredCountriesFromAirport = self.filteredAirport.filter{
            unique.updateValue(false, forKey: String($0.countryId) as String) ?? true
        }
        
    }
}



