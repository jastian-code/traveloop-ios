//
//  FlightSearchResult.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/1/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

struct FlightSearchResult {
    var id: String!
    
    var currencyCode: String
    var totalFareAmt: Double
    var origTotalFareAmt: Double
    var totalServiceCharges: Double
    var allowRefund: Bool
    var stopsNum: Int
    var totalJourneyDuration: Int
    
    var flightSegments : [AirlineFlight]
    
    var totalJourneyDurationDisplay : String{
        get{
            
            let days:Int = totalJourneyDuration/86400000
            let hours = (totalJourneyDuration/3600000) % 24
            let minutes = (totalJourneyDuration/60000) % 60

            let result = days > 0 ? "\(days)d \(hours)h \(minutes)m" : "\(hours)h \(minutes)m"
            
            return result
        }
    }
    
    init(){
        flightSegments = [AirlineFlight]()
        currencyCode = ""
        totalFareAmt = 0
        origTotalFareAmt = 0
        totalServiceCharges = 0
        allowRefund = false
        stopsNum = 0
        totalJourneyDuration = 0
    }
}
