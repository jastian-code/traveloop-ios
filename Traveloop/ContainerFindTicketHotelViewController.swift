//
//  ContainerFindTicketHotelViewController.swift
//  Traveloop
//
//  Created by apple on 9/30/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class ContainerFindTicketHotelViewController: UIViewController {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var flightView: UIView!
    @IBOutlet weak var hotelView: UIView!
    
    var languageDict : [String: String] = [:]
    
    private func setLanguageText(){
        self.segmentedControl.setTitle(languageDict["Flight"]!.uppercaseString, forSegmentAtIndex: 0)
        self.segmentedControl.setTitle(languageDict["Hotel"]!.uppercaseString, forSegmentAtIndex: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageDict = GlobalFunction.getLanguageDict()
        setLanguageText()
        
        self.navigationItem.rightBarButtonItem = NavigationBarCustom.sharedInstance.customLogo()
        
            if self.revealViewController() != nil {
                menuButton.target = self.revealViewController()
                menuButton.action = "revealToggle:"
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                let screenSize = UIScreen.mainScreen().bounds.size.width
                self.revealViewController().rearViewRevealWidth = (screenSize * 80) / 100
              
        }
        
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    
    @IBAction func indexChanged(sender: AnyObject) {
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
                self.flightView.center.x += self.view.bounds.width
                self.hotelView.center.x  += self.view.bounds.width
                }, completion: nil)
        case 1:
            self.hotelView.center.x = self.view.bounds.width + (self.view.bounds.width/2)
            UIView.animateWithDuration(0.3, delay: 0.0, options: .CurveEaseInOut, animations: { () -> Void in
                
                self.flightView.center.x -= (self.view.bounds.width)
                self.hotelView.center.x -= (self.view.bounds.width)
                }, completion: nil)
            
        default:
            break
        }
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
