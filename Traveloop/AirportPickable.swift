//
//  AirportPickable.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/12/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

protocol AirportPickable{
    var airportPickerSelectedValue : String? { get set }
    var airportPickerSelectedValueCity : String? { get set }
    var airportPickerSelectedText : String? {get set}
    var airportPickerTitle : String? { get set}

}