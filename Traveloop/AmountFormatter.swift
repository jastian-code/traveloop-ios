//
//  File.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/4/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class AmountFormatter{
    static func toString(amt: Double) -> String{
        let fmt = NSNumberFormatter()
        fmt.numberStyle = .DecimalStyle
        return fmt.stringFromNumber(amt)!
    }
}