//
//  FindTicketViewController.swift
//  Traveloop
//
//  Created by apple on 9/30/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class FindTicketViewController: UIViewController{
    
//    List TagView
//
//
//    1: ScrollView
//    2: PageControl
//    3: FromDepart Text Field
//    4: ToDestination Text Field
//    5: Depart Time Text Field
//    6: Switch
//    7: Return Field
//    8: Passenger
//    
//    
    enum TagView : Int{
        case ScrollView = 1, PageControl, FromDepart, ToDestination, DepartTime, SwitchReturn, ReturnField, Passenger
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    
    let cellIdentifier = ["carousel", "fromDepart", "toDestination", "departDateAndTime", "return", "returnField" ,"passenger"]
    var returnFieldIsHidden : Bool = true
    
    var scrollView : UIScrollView? {
        didSet{
            let carouselScrollViewSize = scrollView!.frame.size
            scrollView?.contentSize = CGSize(width: carouselScrollViewSize.width * CGFloat(carouselImages.count), height: 0.0)
        }
    }
    var pageControl : UIPageControl?
    var switchReturnField : UISwitch?
    var textFrom: UITextField?
    var textTo: UITextField?
    var dateDepart: UITextField?
    var dateReturn: UITextField?
    
    var carouselImages = [carousel]()
    var carouselImagesView : [UIImageView?] = []

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        scrollView?.delegate = self
        carouselImages = [
            (carousel(image: UIImage(named: "photo1")!, title: "Photo 1", url: NSURL(string: "http://dummyurl")!)),
            (carousel(image: UIImage(named: "photo2")!, title: "Photo 2", url: NSURL(string: "http://dummyurl")!)),
            (carousel(image: UIImage(named: "photo3")!, title: "Photo 3", url: NSURL(string: "http://dummyurl")!))
        ]
        
        let pageCount = carouselImages.count
        pageControl?.currentPage = 0
        pageControl?.numberOfPages = pageCount
        
        for _ in 0..<pageCount {
            carouselImagesView.append(nil)
        }
        
        
        loadCarouselVisiblePages()
 
    }
    
    func loadPage(page : Int){
        if page < 0 || page >= carouselImages.count {
            return
        }
        
        if let carouselView = carouselImagesView[page] {
            //
        }else{
                var frame = scrollView!.bounds
                frame.origin.x = frame.size.width * CGFloat(page)
                frame.origin.y = 0.0
            
                let newPageView = UIImageView(image: carouselImages[page].image)
                newPageView.contentMode = .ScaleAspectFit
                newPageView.frame = frame
                scrollView?.addSubview(newPageView)
            
                carouselImagesView[page] = newPageView
        }
    }
    
    func purgePage(page: Int) {
        if page < 0 || page >= carouselImages.count {
            return
        }
        
        if let carouselImageView = carouselImagesView[page] {
            carouselImageView.removeFromSuperview()
            carouselImagesView[page] = nil
        }
    }
    
    func loadCarouselVisiblePages(){
        if let contentOffsetX = scrollView?.contentOffset.x , pageWidth = scrollView?.frame.size.width {
            let page = Int(floor((contentOffsetX * 2.0 + pageWidth) / (pageWidth * 2.0)))
            
            pageControl!.currentPage = page
            let firstPage = page - 1
            let lastPage = page + 1
            
            for var index = 0; index < firstPage; ++index {
                purgePage(index)
            }
            
            for index in firstPage...lastPage{
                loadPage(index)
            }
            
            for var index = lastPage+1; index < carouselImages.count; ++index {
                purgePage(index)
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    func switchDidChange(sender: UISwitch){
        let indexPath = NSIndexPath(forRow: 5, inSection: 0)
        returnFieldIsHidden =  sender.on == true ? false : true
        
        if returnFieldIsHidden == true {
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Top)
        }else{
            tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .Bottom)
        }
        
    }

    
    func initComponent(){
        scrollView = view.viewWithTag(TagView.ScrollView.rawValue) as? UIScrollView
        pageControl = view.viewWithTag(TagView.PageControl.rawValue) as? UIPageControl
        
        textFrom = view.viewWithTag(TagView.FromDepart.rawValue) as? UITextField
        textFrom?.leftView = UIImageView(image:UIImage(named: "plane_from"))
        textFrom?.leftViewMode = UITextFieldViewMode.Always
        textFrom?.enabled = false
        
        textTo = view.viewWithTag(TagView.ToDestination.rawValue) as? UITextField
        textTo?.leftView = UIImageView(image:UIImage(named: "plane_to"))
        textTo?.leftViewMode = UITextFieldViewMode.Always
        
        dateDepart = view.viewWithTag(TagView.DepartTime.rawValue) as? UITextField
        dateDepart?.leftView = UIImageView(image:UIImage(named: "calendar"))
        dateDepart?.leftViewMode = UITextFieldViewMode.Always
        dateReturn = view.viewWithTag(TagView.ReturnField.rawValue) as? UITextField
        dateReturn?.leftView = UIImageView(image:UIImage(named: "calendar"))
        dateReturn?.leftViewMode = UITextFieldViewMode.Always
        
        switchReturnField = view.viewWithTag(TagView.SwitchReturn.rawValue) as? UISwitch
        switchReturnField?.addTarget(self, action: "switchDidChange:", forControlEvents: .ValueChanged)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}



extension FindTicketViewController : UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellIdentifier.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier[indexPath.row], forIndexPath: indexPath) as? UITableViewCell
        
        
        initComponent()
        loadCarouselVisiblePages()
        
        cell?.hidden = indexPath.row == TagView.DepartTime.rawValue && returnFieldIsHidden == true ? true : false
        return cell!
        
    }
}

extension FindTicketViewController : UITableViewDelegate {
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {

        switch indexPath.row {
        case 0:
            return 200
        case 1:
            return 85
        case 5:
            return returnFieldIsHidden == true ? 0 : 55
        case 6:
            return 350
        default:
            return 55
        }
        
    }
    
//    
//    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if section == 0 {
//            return 10.0
//        }
//        return 1.0
//    }
}

extension FindTicketViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(scrollView: UIScrollView) {
          loadCarouselVisiblePages()
    }
}