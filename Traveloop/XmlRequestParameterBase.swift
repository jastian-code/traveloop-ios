//
//  XmlRequestParameterBase.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/25/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

internal class XmlRequestParameterBase : XmlRequestParameter{
    var apiServiceType : APIServiceType
    var HTTPMethod : HTTPMethodType
    var xmlFileName : String = ""
    var apiURI: String = ""
    var headerParam = [String:String]()
    
    internal init(type : APIServiceType, HTTPMethod : HTTPMethodType){
        self.apiServiceType = type
        self.HTTPMethod = HTTPMethod
        
    }
    
    func replaceXmlValue(var xml: NSString)-> NSString{ return xml }
}
