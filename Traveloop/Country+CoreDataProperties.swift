//
//  Country+CoreDataProperties.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Country {

    @NSManaged var countryIATACode: String?
    @NSManaged var countryISO2CharCode: String?
    @NSManaged var countryISO3CharCode: String?
    @NSManaged var countryISONumberCode: String?
    @NSManaged var countryName: String?
    @NSManaged var id: NSNumber?
    @NSManaged var isActive: NSNumber?
    @NSManaged var createdDate: NSDate?
    @NSManaged var modifiedDate: NSDate?
    @NSManaged var airports: NSSet?
    @NSManaged var cities: NSOrderedSet?

}
