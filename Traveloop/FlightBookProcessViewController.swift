//
//  FlightBookProcessViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/30/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class FlightBookProcessViewController: UITableViewController {
    var requestParam = XmlRequestParameterAirDetailsUpdate()
    
    //Departure (Section 0)
    @IBOutlet weak var lblAirline: UILabel!
    @IBOutlet weak var imgViewAirline: UIImageView!
    @IBOutlet weak var lblFrom: UILabel!
    @IBOutlet weak var lblFromValue: UILabel!
    @IBOutlet weak var lblDepart: UILabel!
    @IBOutlet weak var lblDepartValue: UILabel!
    @IBOutlet weak var lblTo: UILabel!
    @IBOutlet weak var lblToValue: UILabel!
    @IBOutlet weak var lblArrive: UILabel!
    @IBOutlet weak var lblArriveValue: UILabel!
    @IBOutlet weak var lblDuration: UILabel!
    @IBOutlet weak var lblDurationValue: UILabel!
    
    //Return (Section 1)
    @IBOutlet weak var lblAirlineReturn: UILabel!
    @IBOutlet weak var imgViewAirlineReturn: UIImageView!
    @IBOutlet weak var lblFromReturn: UILabel!
    @IBOutlet weak var lblFromValueReturn: UILabel!
    @IBOutlet weak var lblDepartReturn: UILabel!
    @IBOutlet weak var lblDepartValueReturn: UILabel!
    @IBOutlet weak var lblToReturn: UILabel!
    @IBOutlet weak var lblToValueReturn: UILabel!
    @IBOutlet weak var lblArriveReturn: UILabel!
    @IBOutlet weak var lblArriveValueReturn: UILabel!
    @IBOutlet weak var lblDurationReturn: UILabel!
    @IBOutlet weak var lblDurationValueReturn: UILabel!
    
    //Order Total(Section 2)
    @IBOutlet weak var lblPassenger: UILabel!
    @IBOutlet weak var lblPassengerValue: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblPriceValue: UILabel!
    
    //T&C, Button (Section 3)
    @IBOutlet weak var lblTerms: UILabel!
    @IBOutlet weak var switchTerms: UISwitch!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnContinue: UIButton!
    
    var languageDict : [String: String] = [:]
    
    @IBAction func backButton(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    @IBAction func btnFlightDetail(sender: AnyObject) {
        let viewDetails = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightDetailsUserControllerIdentifier) as! FlightDetailsViewController
        
        viewDetails.flight = self.requestParam.flight
        viewDetails.title = self.languageDict["DepartureFlightDetails"]!
        
        let navDetails = UINavigationController(rootViewController: viewDetails)
        self.navigationController?.presentViewController(navDetails, animated: true, completion: nil)
    }
    @IBAction func btnFlightReturnDetail(sender: AnyObject) {
        let viewDetails = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightDetailsUserControllerIdentifier) as! FlightDetailsViewController
        
        viewDetails.flight = self.requestParam.flightReturn
        viewDetails.title = self.languageDict["ReturnFlightDetails"]!
        
        let navDetails = UINavigationController(rootViewController: viewDetails)
        self.navigationController?.presentViewController(navDetails, animated: true, completion: nil)
    }
    
    
    @IBAction func btnContinueTouch(sender: AnyObject) {
        guard self.switchTerms.on else{
            UIAlertView(title: nil, message: self.languageDict["T&CAgree"], delegate: nil, cancelButtonTitle: "OK").show()
            return
        }
        
        UIAlertView(title: nil, message: "Lanjut!", delegate: nil, cancelButtonTitle: "OK").show()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.languageDict = GlobalFunction.getLanguageDict()
        self.title = self.languageDict["Review"]
        self.fillLanguageText()
        self.bindData()
        
        //label.text = requestParam.airSearchFlightDetailXmlResponse
        print(requestParam.flight!.id!)
        print(requestParam.flightReturn?.id!)
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fillLanguageText(){
        self.lblFrom.text = "\(languageDict["From"]!):"
        self.lblDepart.text = "\(languageDict["Depart"]!):"
        self.lblTo.text = "\(languageDict["To"]!):"
        self.lblArrive.text = "\(languageDict["Arrive"]!):"
        self.lblDuration.text = "\(languageDict["Duration"]!):"
        
        if(self.requestParam.isRoundTrip){
            self.lblFromReturn.text = "\(languageDict["From"]!):"
            self.lblDepartReturn.text = "\(languageDict["Depart"]!):"
            self.lblToReturn.text = "\(languageDict["To"]!):"
            self.lblArriveReturn.text = "\(languageDict["Arrive"]!):"
            self.lblDurationReturn.text = "\(languageDict["Duration"]!):"
        }
        
        self.lblPassenger.text = "\(languageDict["Passenger(s)"]!):"
        self.lblPrice.text = "\(languageDict["Price"]!):"
        self.lblTerms.text = languageDict["T&CValue"]!
        
        self.btnCancel.setTitle(languageDict["Cancel&Return"]!, forState: UIControlState.Normal)
        self.btnContinue.setTitle(languageDict["Continue"]!, forState: UIControlState.Normal)
    }
    
    func bindData(){
        var totalPrice : Double = 0
        var currencyCode = ""
        
        if let flight = self.requestParam.flight{
            currencyCode = flight.currencyCode
            totalPrice += flight.origTotalFareAmt
            
            let flightSegment = flight.flightSegments[0]
                
            self.lblAirline.text = "\(flightSegment.airlineName) \(flightSegment.flightNumber)"
            self.imgViewAirline?.contentMode = UIViewContentMode.ScaleAspectFit
            if kAirlineImageIcon[flightSegment.airlineName] != nil{
                if let img = UIImage(named:  kAirlineImageIcon[(flightSegment.airlineName)]!){
                    let imgAirline = ImageHelper.resizeImage(img, newHeight: 20)
                    imgViewAirline?.frame = CGRect(x: imgViewAirline!.frame.origin.x, y: imgViewAirline!.frame.origin.y, width: imgAirline.size.width, height: imgAirline.size.height)
                    imgViewAirline?.image = imgAirline
                }
            }
            
        
            if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegment.originAirportCode){
                self.lblFromValue.text = "(\(flightSegment.originAirportCode)) \(airport.airportName!)"
            }else{
                self.lblFromValue.text = flightSegment.originAirportCode
            }
            
            self.lblDepartValue.text = "\(self.requestParam.departureDateDisplay) | \(flightSegment.departureTimeDisplay)"
            
            let flightSegmentLast = flight.flightSegments.count > 1 ? flight.flightSegments.last! : flightSegment
            if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegmentLast.arrivalAirportCode){
                self.lblToValue.text = "(\(flightSegmentLast.arrivalAirportCode)) \(airport.airportName!)"
            }else{
                self.lblToValue.text = flightSegmentLast.arrivalAirportCode
            }
            

            let arrivalDate = DateUtility.dateFromString(flightSegmentLast.arrivalDate, format: "yyyy-MM-dd")
            let cvArrivalDate = CVDate(date: arrivalDate!)
            self.lblArriveValue.text = "\(cvArrivalDate.customDescriptionText) | \(flightSegmentLast.arrivalTimeDisplay)"
            
            self.lblDurationValue.text = flight.totalJourneyDurationDisplay
        }
        
        
        if(self.requestParam.isRoundTrip){
            if let flight = self.requestParam.flightReturn{
                totalPrice += flight.origTotalFareAmt
                
                let flightSegment = flight.flightSegments[0]
                
                self.lblAirlineReturn.text = "\(flightSegment.airlineName) \(flightSegment.flightNumber)"
                self.imgViewAirlineReturn?.contentMode = UIViewContentMode.ScaleAspectFit
                if kAirlineImageIcon[flightSegment.airlineName] != nil{
                    if let img = UIImage(named:  kAirlineImageIcon[(flightSegment.airlineName)]!){
                        let imgAirline = ImageHelper.resizeImage(img, newHeight: 20)
                        imgViewAirlineReturn?.frame = CGRect(x: imgViewAirline!.frame.origin.x, y: imgViewAirline!.frame.origin.y, width: imgAirline.size.width, height: imgAirline.size.height)
                        imgViewAirlineReturn?.image = imgAirline
                    }
                }
                
                
                if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegment.originAirportCode){
                    self.lblFromValueReturn.text = "(\(flightSegment.originAirportCode)) \(airport.airportName!)"
                }else{
                    self.lblFromValueReturn.text = flightSegment.originAirportCode
                }
                
                self.lblDepartValueReturn.text = "\(self.requestParam.returnDateDisplay) | \(flightSegment.departureTimeDisplay)"
                
                let flightSegmentLast = flight.flightSegments.count > 1 ? flight.flightSegments.last! : flightSegment
                if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegmentLast.arrivalAirportCode){
                    self.lblToValueReturn.text = "(\(flightSegmentLast.arrivalAirportCode)) \(airport.airportName!)"
                }else{
                    self.lblToValueReturn.text = flightSegmentLast.arrivalAirportCode
                }
                
                let arrivalDate = DateUtility.dateFromString(flightSegmentLast.arrivalDate, format: "yyyy-MM-dd")
                let cvArrivalDate = CVDate(date: arrivalDate!)
                self.lblArriveValueReturn.text = "\(cvArrivalDate.customDescriptionText) | \(flightSegmentLast.arrivalTimeDisplay)"
                
                self.lblDurationValueReturn.text = flight.totalJourneyDurationDisplay
            }
        }
        
        self.lblPassengerValue.text = "\(self.requestParam.adultPassengerCount) \(self.languageDict["Adult(s)"]!)"
        if(self.requestParam.childPassengerCount > 0){
            if(self.requestParam.childPassengerCount > 1){
                lblPassengerValue.text! += ", \(self.requestParam.childPassengerCount) \(self.languageDict["Children"]!)"
            }else{
                lblPassengerValue.text! += ", \(self.requestParam.childPassengerCount) \(self.languageDict["Child"]!)"
            }
        }
        if(self.requestParam.infantPassengerCount > 0){
            lblPassengerValue.text! += ", \(self.requestParam.infantPassengerCount) \(self.languageDict["Infant(s)"]!)"
        }
        
        
        self.lblPriceValue.text = "\(currencyCode) \(AmountFormatter.toString(totalPrice))"
    }
}


//tableView appearance
extension FlightBookProcessViewController{
   
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
       
        switch indexPath.section{
            case 0: return 147
            case 1: return (!self.requestParam.isRoundTrip && indexPath.section == 1) ? 0.1 : 147
            case 2: return 52
            case 3: return 110
            default:  return tableView.rowHeight
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if (!self.requestParam.isRoundTrip && section == 1){
            return 0.1
        }else if(section == 3){
            return 10
        }else{
            return 30
        }
        
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if (!self.requestParam.isRoundTrip && section == 1){
            return nil
        } else{
            var sectionText = ""
            switch section{
                case 0 : sectionText = "\(self.requestParam.originDisplay) - \(self.requestParam.destinationDisplay)";break;
                case 1 : sectionText = "\(self.requestParam.destinationDisplay) - \(self.requestParam.originDisplay)";break;
                case 2 : sectionText = self.languageDict["OrderTotal"]!;break;
                default: break;
            }
            
            
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 1))
            
            let label = UILabel(frame: CGRectMake(10, 0, UIScreen.mainScreen().bounds.width, 30))
            
            label.textColor = UIColor.blackColor()
            label.numberOfLines = 1
            label.font = UIFont(name: label.font.fontName, size: 15)
            label.textAlignment = NSTextAlignment.Left
            label.text = sectionText
            
            headerView.addSubview(label)
            
            return headerView
        }
        
    }
    
    
}
