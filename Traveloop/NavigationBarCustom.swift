//
//  NavigationCustom.swift
//  Traveloop
//
//  Created by Apple on 9/28/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class NavigationBarCustom {
    class var sharedInstance : NavigationBarCustom {
        struct Singleton {
            static let instance = NavigationBarCustom()
        }

            return Singleton.instance
    }
    
    func customBackground() {
        
        let navigationBarAppearance = UINavigationBar.appearance()
        let backgroundNavigationBar = UIImage(named: "background_toolbar")
        
        navigationBarAppearance.tintColor = UIColor.whiteColor()
        navigationBarAppearance.setBackgroundImage(backgroundNavigationBar, forBarPosition: UIBarPosition.Top, barMetrics: .Default)
        navigationBarAppearance.contentMode = UIViewContentMode.Right
        navigationBarAppearance.titleTextAttributes = [NSForegroundColorAttributeName:UIColor.whiteColor()]
        navigationBarAppearance.translucent = false
        
        UIApplication.sharedApplication().setStatusBarStyle(.LightContent, animated: true)
    }
    
    func customLogo() -> UIBarButtonItem {
        let navigationLogo  = UIBarButtonItem(title: "Traveloop", style: UIBarButtonItemStyle.Done, target: self, action: nil)
        if let fontLogo = UIFont(name: "Lobster1.4", size: 30) {
            navigationLogo.setTitleTextAttributes([NSFontAttributeName: fontLogo], forState: .Normal)
        }
       return navigationLogo
    }
}
