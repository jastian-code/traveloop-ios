    //
//  LoginVieViewController.swift
//  Traveloop
//
//  Created by Apple on 9/28/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit
    
    class LoginViewController: UIViewController {
        
        @IBOutlet weak var txtEmail: UITextField!
        @IBOutlet weak var txtPassword: UITextField!
        @IBOutlet weak var btnSignIn: UIButton!
        @IBOutlet weak var btnForgotPassword: UIButton!
        @IBOutlet weak var btnCreateAccount: UIButton!
        
        var languageDict : [String: String] = [:]
        
        @IBOutlet weak var menuButton: UIBarButtonItem!
        override func viewDidLoad() {
            super.viewDidLoad()
            
            self.languageDict = GlobalFunction.getLanguageDict()
            setLanguageText()
            
            self.view.backgroundColor = UIColor(patternImage: UIImage(named:  "background_login")!)
            self.navigationItem.rightBarButtonItem = NavigationBarCustom.sharedInstance.customLogo()
            
            if self.revealViewController() != nil {
                menuButton.target = self.revealViewController()
                menuButton.action = "revealToggle:"
                self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
                let screenSize = UIScreen.mainScreen().bounds.size.width
                self.revealViewController().rearViewRevealWidth = (screenSize * 80) / 100
            }
        }
        
        private func setLanguageText(){
            txtEmail.placeholder = self.languageDict["Email"]!
            txtPassword.placeholder = self.languageDict["Password"]!
            btnSignIn.setTitle(self.languageDict["SignIn"]!, forState: .Normal)
            btnForgotPassword.setTitle(self.languageDict["ForgotPassword"]!, forState: .Normal)
            btnCreateAccount.setTitle(self.languageDict["CreateAccount"]!, forState: .Normal)
        }
        
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
        
        /*
        // MARK: - Navigation
        
        // In a storyboard-based application, you will often want to do a little preparation before navigation
        override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        }
        */
        
    }
