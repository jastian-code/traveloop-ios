//
//  Constant.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/23/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

public struct TraveloopAPIUri{
    static var host = "http://119.81.78.218"
    static var flightSearch = "\(host)/b2bp/service/api/flight"
    static var getAirports = "http://172.26.100.101/static/Airports.json?v=1"
    static var getCountries = "http://172.26.100.101/static/Countries.json?v=1"
    static var getCities = "http://172.26.100.101/static/Cities.json?v=1"
}

public enum APIResponseType : String{
    case json = "json"
    case xml = "xml"
}

public enum APIServiceType : String{
    case AirSearch = "AirSearch"
    case AirDetailsUpdate = "AirDetailsUpdate"
    case AirBook = "AirBook"
}

public enum HTTPMethodType : String{
    case GET = "GET"
    case POST = "POST"
    case PUT = "PUT"
    case DELETE = "DELETE"
}

public enum Language: String{
    case EN = "en"
    case ID = "id"
}

//User Defaults
let kSelectedLanguageUserDefault = "com.codeid.traveloop.selectedLanguage"
let kTimeStampUserDefault = "com.codeid.traveloop.timeStamp"

//Default Value
let kFlightOriginText = "(CGK) Jakarta"
let kFlightOriginValue = "CGK"
let kFlightDestinationText = "(DPS) Bali"
let kFlightDestinationValue = "DPS"


//Identifier
let kFlightResultUserControllerIdentifier = "FlightSearchResult"
let kFlightDetailsUserControllerIdentifier = "FlightDetails"
let kFlightBookProcessUserControllerIdentifier = "FlightBookProcess"

//AirlineImageIcon
let kAirlineImageIcon = [
    "AirAsia" : "airasia",
    "Batavia Air" : "bataviaair",
    "Cebu Pacific" : "cebupacific",
    "Batik Air" : "batikair",
    "Citilink" : "citilink",
    "Garuda" : "garuda",
    "Jetstar" : "jetstar",
    "Lion Air" : "lionair",
    "Mandala" : "mandala",
    "Sriwijaya Air" : "sriwijayaair",
    "Tiger Airways" : "tigerairways",
    "Trigana Air" : "triganaair"
]

//Pallete
let kColorBlue = UIColor.colorFromCode(0x00A3E4)
let kColorPinkGrey = UIColor.colorFromCode(0xFB8DA6)

//language
let kLanguageDefault = "id"
let kLanguageDictDefault = kLanguageDictID
let kLanguageDictEn = [
    
    "Account" : "Account",
    "Adult(s)" : "Adult(s)",
    "Adult" : "Adult",
    "Arrival" : "Arrival",
    "Arrive" : "Arrive",
    
    "Cancel" : "Cancel",
    "Cancel&Return" : "     Cancel &\nReturn to Search",
    "Child" : "Child",
    "Children" : "Children",
    "Contact" : "Contact",
    "Continue" : "Continue",
    "CreateAccount" : "Create Account",
    
    "Date" : "Date",
    "Depart" : "Depart",
    "Departure" : "Departure",
    "DepartDate" : "Depart Date",
    "DepartureFlight" : "Departure Flight",
    "DepartureFlightDetails" : "Departure Flight Details",
    "Destination" : "Destination",
    "Detail" : "Detail",
    "Details" : "Details",
    "Direct" : "Direct",
    "Duration" : "Duration",
    
    "Email" : "Email",
    "Error-1009": "The Internet connection appears to be offline.\nPlease check your internet connection",
    "E-Ticket" : "E-Ticket",
    
    "FAQ" : "FAQ",
    "FindFlight&Hotel" : "Find Flight & Hotel",
    "Flight" : "Flight",
    "FlightsNotAvailable" : "Flights not available",
    "ForgotPassword" : "Forgot Password",
    "From" : "From",
    
    "Help" : "Help",
    "Hotel" : "Hotel",
    
    "Infant" : "Infant",
    "Infant(s)" : "Infant(s)",
    
    "MyBooking" : "My Booking",
    
    "NotRefundable" : "Not Refundable",
    
    "LogIn" : "Log In",
    
    "Order" : "Order",
    "OrderTotal" : "Order Total",
    "Origin" : "Origin",
    
    "Passenger(s)" : "Passenger(s)",
    "Password" : "Password",
    "Price" : "Price",
    
    "Refundable" : "Refundable",
    "Return" : "Return",
    "ReturnDate" : "Return Date",
    "ReturnFlightDetails" : "Return Flight Details",
    "ReturnToSearch" : "Return to Search",
    "Review" : "Review",
    "RoundTrip" : "Round Trip",
    
    "Search" : "Search",
    "SearchFlight" : "Search Flight",
    "SignIn" : "Sign In",
    "Submit" : "Submit",
    
    "TermsAndCondition" : "Terms and Condition",
    "To" : "To",
    "Total" : "Total",
    "Transit" : "Transit",
    "Transits": "Transits",
    "T&CAgree" : "Please agree terms and condition before continue",
    "T&CValue" : "I have read and accept the rules and restrictions",
    
    "Years" : "Years",
    
    //DayOfWeeks
    "Sunday" : "Sunday",
    "Monday" : "Monday",
    "Tuesday" : "Tuesday",
    "Wednesday" : "Wednesday",
    "Thursday" : "Thursday",
    "Friday" : "Friday",
    "Saturday" : "Saturday"
]
let kLanguageDictID = [
    
    "Account" : "Akun",
    "Adult" : "Dewasa",
    "Adult(s)" : "Dewasa",
    "Arrival" : "Kedatangan",
    "Arrive" : "Tiba",
    
    "Cancel" : "Batal",
    "Cancel&Return" : "             Batal &\nKembali ke Pencarian",
    "Child" : "Anak",
    "Children" : "Anak",
    "Contact" : "Kontak",
    "Continue" : "Lanjut",
    "CreateAccount" : "Buat Akun",
    
    "Date" : "Tanggal",
    "Depart" : "Berangkat",
    "DepartDate" : "Tanggal Pergi",
    "Departure" : "Keberangkatan",
    "DepartureFlight" : "Penerbangan Pergi",
    "DepartureFlightDetails" : "Rincian Keberangkatan",
    "Detail" : "Rincian",
    "Details" : "Rincian",
    "Destination" : "Tujuan",
    "Direct" : "Langsung",
    "Duration" : "Durasi",
    
    "Email" : "Email",
    "Error-1009": "Tidak ada sambungan ke internet.\nMohon periksa sambungan internet",
    "E-Ticket" : "E-Tiket",
    
    "FAQ" : "FAQ",
    "FindFlight&Hotel" : "Cari Penerbangan & Hotel",
    "Flight" : "Penerbangan",
    "FlightsNotAvailable" : "Penerbangan tidak ditemukan",
    "ForgotPassword" : "Lupa Sandi",
    "From" : "Dari",
    
    "Help" : "Bantuan",
    "Hotel" : "Hotel",
    
    "Infant" : "Bayi",
    "Infant(s)" : "Bayi",
    
    "LogIn" : "Masuk",
    
    "MyBooking" : "Pesanan Saya",
    
    "NotRefundable" : "Not Refundable",
    
    "Order" : "Pesanan",
    "OrderTotal" : "Total Pesanan",
    "Origin" : "Asal",
    
    "Passenger(s)" : "Penumpang",
    "Password" : "Sandi",
    "Price" : "Harga",
    
    "Refundable" : "Refundable",
    "Return" : "Kembali",
    "ReturnDate" : "Tanggal Pulang",
    "ReturnFlight" : "Penerbangan Kembali",
    "ReturnFlightDetails" : "Rincian Penerbangan Kembali",
    "ReturnToSearch" : "Kembali ke Pencarian",
    "Review" : "Ulasan",
    "RoundTrip" : "Pulang Pergi",
    
    "Search" : "Cari",
    "SearchFlight" : "Cari Penerbangan",
    "SignIn" : "Masuk",
    "Submit" : "Submit",
    
    "Transit" : "Transit",
    "Transits": "Transits",
    "TermsAndCondition" : "Syarat dan Ketentuan",
    "To" : "Ke",
    "Total" : "Total",
    "T&CAgree" : "Mohon menyetujui syarat dan ketentuan sebelum melanjutkan",
    "T&CValue" : "Saya sudah membaca dan setuju dengan aturan dan pembatasan",
    
    "Years" : "Tahun",
    
    //DayOfWeeks
    "Sunday" : "Minggu",
    "Monday" : "Senin",
    "Tuesday" : "Selasa",
    "Wednesday" : "Rabu",
    "Thursday" : "Kamis",
    "Friday" : "Jumat",
    "Saturday" : "Sabtu"
]