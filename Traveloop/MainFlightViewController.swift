//
//  MainFlightViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/6/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//
import UIKit

class MainFlightViewController: UIViewController, CalendarPickable,  AirportPickable{
    var calendarPickerSelectedDate: NSDate?
    var calendarPickerTitle : String?
    var calendarPickerSelectedValue : String?{
        didSet{
            if let val = calendarPickerSelectedValue{
                if let title = calendarPickerTitle{
                    switch title{
                    case "DepartDate":
                        self.requestParam.departureDate = calendarPickerSelectedDate
                        self.requestParam.departureDateString = val;
                        break
                    case "ReturnDate":
                        self.requestParam.departureDate = calendarPickerSelectedDate
                        self.requestParam.returnDateString = val; break
                    default: break
                    }
                }
            }
        }

    }
    var calendarPickerSelectedText : String?{
        didSet{
            if let val = calendarPickerSelectedText{
                if let title = calendarPickerTitle{
                    switch title{
                        case "DepartDate":
                            labelDepart?.text = val;break;
                        case "ReturnDate":
                            labelReturn?.text = val;break;
                        default: break
                    }
                }
                
            }
        }
    }
    
    var airportPickerTitle : String?
    var airportPickerSelectedValueCity : String?{
        didSet{
            if let val = airportPickerSelectedValueCity{
                if let title = airportPickerTitle{
                    switch title{
                    case "Origin":
                        self.requestParam.originCity = val; break
                    case "Destination":
                        self.requestParam.destinationCity = val; break
                    default: break
                    }
                }
                
            }
        }
    }
    var airportPickerSelectedValue : String?{
        didSet{
            if let val = airportPickerSelectedValue{
                if let title = airportPickerTitle{
                    switch title{
                    case "Origin":
                        self.requestParam.originAirportCode = val; break
                    case "Destination":
                        self.requestParam.destinationAirportCode = val; break
                    default: break
                    }
                }
                
            }
        }
    }

    var airportPickerSelectedText : String?{
        didSet{
            if let val = airportPickerSelectedText{
                if let title = airportPickerTitle{
                    switch title{
                        case "Origin":labelFrom?.text = val; break
                        case "Destination":labelTo?.text = val; break
                        default: break
                    }
                }
                
            }
        }
    }
    
    
    //    List TableView TagView
    //
    //    1: labelFrom
    //    2: labelTo
    //    3: labelDepart
    //    4: switchRoundTrip
    //    5: labelReturn
    //   11: labelPassengerAdult
    //   12: stepperPassengerAdult
    //   21: labelPassengerChild
    //   22: stepperPassengerChild
    //   31: labelPassengerInfant
    //   32: stepperPassengerInfant
    //  100: btnSearch
    
    enum TagView : Int{
        case LabelFrom = 1, LabelTo, LabelDepart, SwitchRoundTrip, LabelReturn,
        labelPassengerAdult = 11, stepperPassengerAdult,
        labelPassengerChild = 21, stepperPassengerChild,
        labelPassengerInfant = 31, stepperPassengerInfant,
        btnSearch = 100
    }
    
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = ["cellFrom", "cellTo", "cellDepart", "cellRoundTrip", "cellReturn", "cellPassengers", "cellSearch", "cellEmpty" ]
    var returnFieldIsHidden : Bool = true
    
    
    var lblOriginText, lblDestinationText, lblDepartText, lblRoundTripText, lblReturnText, lblPassengerText, lblAdultsText, lblChildrenText, lblInfantText : UILabel?
    
    var labelFrom, labelTo, labelDepart, labelReturn : UILabel?
    var switchRoundTrip : UISwitch?
    var labelPassengerAdult, labelPassengerChild, labelPassengerInfant : UILabel?
    var stepperPassengerAdult, stepperPassengerChild, stepperPassengerInfant : UIStepper?
    var btnSearch : UIButton?
    let requestParam = XmlRequestParameterAirSearch()
    var shouldInitValue = true
    
    var languageDict : [String:String] = [:]
    
    @IBAction func unwindToMainFlight(unwindSegue: UIStoryboardSegue){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageDict = GlobalFunction.getLanguageDict()
        
    }
    
    private func setLanguageText(){
        lblOriginText = view.viewWithTag(101) as? UILabel
        lblDestinationText = view.viewWithTag(102) as? UILabel
        lblDepartText = view.viewWithTag(103) as? UILabel
        lblRoundTripText = view.viewWithTag(104) as? UILabel
        lblReturnText = view.viewWithTag(105) as? UILabel
        lblPassengerText = view.viewWithTag(106) as? UILabel
        lblAdultsText = view.viewWithTag(107) as? UILabel
        lblChildrenText = view.viewWithTag(108) as? UILabel
        lblInfantText = view.viewWithTag(109) as? UILabel
        
        lblOriginText?.text = languageDict["Origin"]!
        lblDestinationText?.text = languageDict["Destination"]!
        lblDepartText?.text = languageDict["Depart"]!
        lblRoundTripText?.text = "\(languageDict["RoundTrip"]!)?"
        lblReturnText?.text = languageDict["Return"]!
        lblPassengerText?.text = languageDict["Passenger(s)"]!
        lblAdultsText?.text = "\(languageDict["Adult(s)"]!)\n(> 12 \(languageDict["Years"]!))"
        lblChildrenText?.text = "\(languageDict["Children"]!)\n(2-12 \(languageDict["Years"]!))"
        lblInfantText?.text = "\(languageDict["Infant"]!)\n(0-12 \(languageDict["Years"]!))"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func switchRoundTripDidChange(sender: UISwitch){
        let cellReturnIndexPath = NSIndexPath(forRow: cellIdentifier.indexOf("cellReturn")!, inSection:0)
        
        returnFieldIsHidden = !sender.on
        
        tableView.reloadRowsAtIndexPaths([cellReturnIndexPath], withRowAnimation: returnFieldIsHidden ? .Top : .Bottom)
        tableView.cellForRowAtIndexPath(cellReturnIndexPath)?.hidden = returnFieldIsHidden
        
        
        if sender.on{
            sender.onTintColor = UIColor.orangeColor()
            if self.requestParam.returnDate == nil || self.requestParam.returnDate!.isLessThanDate(self.requestParam.departureDate!){
                
                self.requestParam.returnDate = self.requestParam.departureDate!.addDays(1)
                self.requestParam.returnDateString = DateUtility.stringFromDate(self.requestParam.returnDate!, format: "yyyy-MM-dd")
                
                calendarPickerTitle = "ReturnDate"
                calendarPickerSelectedText = DateUtility.stringFromDate(self.requestParam.returnDate!, format: "EEEE, dd MMM yyyy")
            }
        }
        else{
            sender.onTintColor = UIColor.grayColor()
        }
        
    }
    
    func stepperPassengerAdultDidChange(sender: UIStepper){
        labelPassengerAdult!.text = String(format:"%.0f", sender.value)
        
    }
    func stepperPassengerChildDidChange(sender: UIStepper){
        labelPassengerChild!.text = String(format:"%.0f", sender.value)
        
    }
    func stepperPassengerInfantDidChange(sender: UIStepper){
       labelPassengerInfant!.text = String(format:"%.0f", sender.value)
        
    }
    
    
    
    func initComponent(){
        setLanguageText()
        
        labelFrom = view.viewWithTag(TagView.LabelFrom.rawValue) as? UILabel
        labelTo = view.viewWithTag(TagView.LabelTo.rawValue) as? UILabel
        
       
        labelDepart = view.viewWithTag(TagView.LabelDepart.rawValue) as? UILabel
        labelReturn = view.viewWithTag(TagView.LabelReturn.rawValue) as? UILabel
      
        
        switchRoundTrip = view.viewWithTag(TagView.SwitchRoundTrip.rawValue) as? UISwitch
        switchRoundTrip?.addTarget(self, action: "switchRoundTripDidChange:", forControlEvents: .ValueChanged)
        
        labelPassengerAdult = view.viewWithTag(TagView.labelPassengerAdult.rawValue) as? UILabel
        labelPassengerChild = view.viewWithTag(TagView.labelPassengerChild.rawValue) as? UILabel
        labelPassengerInfant = view.viewWithTag(TagView.labelPassengerInfant.rawValue) as? UILabel
        stepperPassengerAdult = view.viewWithTag(TagView.stepperPassengerAdult.rawValue) as? UIStepper
        stepperPassengerAdult?.transform = CGAffineTransformMakeScale(0.80, 0.80)
        stepperPassengerAdult?.addTarget(self, action: "stepperPassengerAdultDidChange:", forControlEvents: .ValueChanged)
        stepperPassengerChild = view.viewWithTag(TagView.stepperPassengerChild.rawValue) as? UIStepper
        stepperPassengerChild?.transform = CGAffineTransformMakeScale(0.80, 0.80)
        stepperPassengerChild?.addTarget(self, action: "stepperPassengerChildDidChange:", forControlEvents: .ValueChanged)
        stepperPassengerInfant = view.viewWithTag(TagView.stepperPassengerInfant.rawValue) as? UIStepper
        stepperPassengerInfant?.transform = CGAffineTransformMakeScale(0.80, 0.80)
        stepperPassengerInfant?.addTarget(self, action: "stepperPassengerInfantDidChange:", forControlEvents: .ValueChanged)
        
        btnSearch = view.viewWithTag(TagView.btnSearch.rawValue) as? UIButton
        btnSearch?.setTitle(languageDict["SearchFlight"]!, forState: .Normal)
        
    }
    
    func initValue(){
        //init value
        let today = NSDate()
        
        self.requestParam.originAirportCode = kFlightOriginValue
        self.requestParam.destinationAirportCode = kFlightDestinationValue
        self.requestParam.departureDate = today
        self.requestParam.departureDateString = DateUtility.stringFromDate(today, format: "yyyy-MM-dd")
       
        airportPickerTitle = "Origin"
        airportPickerSelectedText = kFlightOriginText
        airportPickerTitle = "Destination"
        airportPickerSelectedText = kFlightDestinationText
        calendarPickerTitle = "DepartDate"
        calendarPickerSelectedText = DateUtility.stringFromDate(today, format: "EEEE, dd MMM yyyy")
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let navViewController = segue.destinationViewController as? UINavigationController{
            if var calendarPicker = navViewController.topViewController as? CalendarPickable{
                if segue.identifier == "CalendarPickerDepartSegue"{
                    calendarPickerTitle = "DepartDate"
  
                    
                }
                else if segue.identifier == "CalendarPickerReturnSegue"{
                    calendarPickerTitle = "ReturnDate"

                    
                }
                calendarPicker.calendarPickerTitle = calendarPickerTitle
                calendarPicker.calendarPickerSelectedValue = calendarPickerSelectedValue
                calendarPicker.calendarPickerSelectedText = calendarPickerSelectedText
                
            }
            else if var airportPicker = navViewController.topViewController as? AirportPickable{
                if segue.identifier == "AirportPickerOriginSegue"{
                    airportPickerTitle = "Origin"

                }
                else if segue.identifier == "AirportPickerDestinationSegue"{
                    airportPickerTitle = "Destination"

                    
                }
                airportPicker.airportPickerTitle = airportPickerTitle
                airportPicker.airportPickerSelectedValue = airportPickerSelectedValue
                airportPicker.airportPickerSelectedValueCity = airportPickerSelectedValueCity
                airportPicker.airportPickerSelectedText = airportPickerSelectedText
            }
        }
        else if let searchResultViewCtrl = segue.destinationViewController as? FlightSearchResultViewController{
                
                self.requestParam.originDisplay = self.labelFrom!.text!
                self.requestParam.destinationDisplay = self.labelTo!.text!
                self.requestParam.departureDateDisplay = self.labelDepart!.text!
                self.requestParam.returnDateDisplay = self.labelReturn!.text!
            
                self.requestParam.isRoundTrip = switchRoundTrip!.on
                self.requestParam.adultPassengerCount = Int(labelPassengerAdult!.text!)!
                self.requestParam.childPassengerCount = Int(labelPassengerChild!.text!)!
                self.requestParam.infantPassengerCount = Int(labelPassengerInfant!.text!)!
                
                searchResultViewCtrl.requestParam = self.requestParam
            
                let label = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 44))
                label.backgroundColor = UIColor.clearColor()
                label.textColor = UIColor.whiteColor()
                label.numberOfLines = 2
                label.font = UIFont(name: label.font.fontName, size: 11)
                label.textAlignment = NSTextAlignment.Left
                label.text = "\(languageDict["DepartureFlight"]!) - \(self.labelDepart!.text!)\n\(self.labelFrom!.text!) - \(self.labelTo!.text!)"

                searchResultViewCtrl.navigationItem.titleView = label
            
        }
    }
    
    
    
    @IBAction func SearchSubmit(sender: UIButton) {
        
                
    }
        
    
}


extension MainFlightViewController : UITableViewDataSource {
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellIdentifier.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier[indexPath.row], forIndexPath: indexPath) as? UITableViewCell
        
        
        initComponent()
        if shouldInitValue && indexPath.row == cellIdentifier.indexOf("cellSearch"){
            
            initValue()
            shouldInitValue = false
        }
    
        cell!.hidden = indexPath.row == cellIdentifier.indexOf("cellReturn") && returnFieldIsHidden == true ? true : false
        cell!.selectionStyle = UITableViewCellSelectionStyle.None
        
        return cell!
        
    }
}

extension MainFlightViewController : UITableViewDelegate {
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        let defaultHeight = tableView.rowHeight
        switch indexPath.row {
        
        case cellIdentifier.indexOf("cellReturn")!:
            return returnFieldIsHidden == true ? 0 : defaultHeight
        case cellIdentifier.indexOf("cellPassengers")!:
            return 145
        case cellIdentifier.indexOf("cellSearch")!:
            return 60
        default:
            return defaultHeight
        }
        
    }
    
    //Resolve ios8 issue, segue has delay when trigger in cell of table view
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        dispatch_async(dispatch_get_main_queue()){ () -> Void in
            if indexPath == self.cellIdentifier.indexOf("cellDepart"){
                self.performSegueWithIdentifier("CalendarPickerDepartSegue", sender: nil)
            }else if indexPath == self.cellIdentifier.indexOf("cellReturn"){
                self.performSegueWithIdentifier("CalendarPickerReturnSegue", sender: nil)
            }
        }
    }
}

