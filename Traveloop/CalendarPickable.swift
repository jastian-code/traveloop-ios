//
//  CalendarPickerProtocol.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/11/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

protocol CalendarPickable{
    var calendarPickerSelectedDate : NSDate? { get set }
    var calendarPickerSelectedValue : String? { get set }
    var calendarPickerSelectedText : String? {get set}
    var calendarPickerTitle : String? { get set}
    
}
