//
//  CoreDataDeserializable.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/18/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

protocol CoreDataDeserializable{
    var apiUrl : String {get set}
    func deserialize(context:NSManagedObjectContext, json : JSON) -> [NSManagedObject]
}