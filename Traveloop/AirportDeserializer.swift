//
//  AirportAPIConsumer.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

class AirportDeserializer: CoreDataDeserializable{
    var apiUrl = TraveloopAPIUri.getAirports
    
    func deserialize(context: NSManagedObjectContext, json : JSON) -> [NSManagedObject]{
        var airports = [Airport]()
        let jsonResult = json["Airports"]
        var isCreate: Bool
        
        for(_, subJson) : (String, JSON) in jsonResult{
            
            let id = subJson["id"].intValue
            
            var airport = Airport.findById(context, id: id)
            if airport == nil{
                airport = Airport.create(context)
                airport!.id = id
                isCreate = true
            }else{
                isCreate = false
            }
            
            
            airport!.airportCode = subJson["airportCode"].stringValue
            airport!.airportName = subJson["airportName"].stringValue
            airport!.isActive = subJson["isActive"].boolValue
            
            if let createdDate = subJson["createdDate"].stringValue as? String{
                airport!.createdDate = DateUtility.dateFromString(createdDate, format: "yyyy/MM/dd")
            }
            if let modifiedDate = subJson["modifiedDate"].stringValue as? String{
                airport!.modifiedDate = DateUtility.dateFromString(modifiedDate, format: "yyyy/MM/dd")
            }
            
            if let fk = subJson["FKIATACityId"].intValue as? Int{
                if let city = City.findById(context, id: fk){
                    airport!.city = city
                    
                    let airports = airport!.city?.airports?.mutableOrderedSetValueForKey("airports")
                    
                    if isCreate{
                        airports!.addObject(airport!)
                    }
                }
                
            }
            
            airports.append(airport!)
        }
        
        return airports
            
    }
}