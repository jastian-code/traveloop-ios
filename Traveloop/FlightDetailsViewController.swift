//
//  FlightDetailsViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/7/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class FlightDetailsViewController: UIViewController {

    var flight : FlightSearchResult?
   
    
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblOriginDest: UILabel!
   
    @IBAction func doneTouchUp(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if flight != nil{
            if let flightSegment = flight!.flightSegments[0] as? AirlineFlight{
                
                lblOriginDest.text = "\(flightSegment.originCityCode) - \(flight!.flightSegments[flight!.stopsNum].arrivalCityCode)"
                
                lblPrice.text = "\(flight!.currencyCode) \(AmountFormatter.toString(flight!.origTotalFareAmt))"
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


extension FlightDetailsViewController: UITableViewDataSource{

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.flight != nil ? (self.flight?.flightSegments.count)! : 0
    }
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cellSegment", forIndexPath: indexPath) as? UITableViewCell
        
        if self.flight != nil{
            let flightSegment = self.flight?.flightSegments[indexPath.row]
            
            let lblAirline = cell?.viewWithTag(1) as? UILabel
            lblAirline?.text = "\(flightSegment!.airlineName) \(flightSegment!.flightNumber)"
            let imgViewAirline = cell?.viewWithTag(2) as? UIImageView
            imgViewAirline?.contentMode = UIViewContentMode.ScaleAspectFit
            
            if kAirlineImageIcon[flightSegment!.airlineName] != nil{
                if let img = UIImage(named:  kAirlineImageIcon[(flightSegment!.airlineName)]!){
                    let imgAirline = ImageHelper.resizeImage(img, newHeight: 20)
                    imgViewAirline?.frame = CGRect(x: imgViewAirline!.frame.origin.x, y: imgViewAirline!.frame.origin.y, width: imgAirline.size.width, height: imgAirline.size.height)
                    imgViewAirline?.image = imgAirline
                }
            }
            
            
            let lblDepartTime = cell?.viewWithTag(3) as? UILabel
            lblDepartTime?.text = flightSegment!.departureTimeDisplay
            let lblDepartCity = cell?.viewWithTag(4) as? UILabel
            lblDepartCity?.text = "\(flightSegment!.originCityCode) (\(flightSegment!.originAirportCode))"
            let lblDepartAirport = cell?.viewWithTag(5) as? UILabel
            if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegment!.originAirportCode){
                lblDepartAirport?.text = airport.airportName!
            }
            
            
            let lblArrivalTime = cell?.viewWithTag(6) as? UILabel
            lblArrivalTime?.text = flightSegment!.arrivalTimeDisplay
            let lblArivalCity = cell?.viewWithTag(7) as? UILabel
            lblArivalCity?.text = "\(flightSegment!.arrivalCityCode) (\(flightSegment!.arrivalAirportCode))"
            let lblArrivalAirport = cell?.viewWithTag(8) as? UILabel
            if let airport = Airport.findByCode(ContextManager.getContext(), airportCode: flightSegment!.arrivalAirportCode){
                lblArrivalAirport?.text = airport.airportName!
            }
           
        }
        
        
        return cell!
    }
    
}

extension FlightDetailsViewController : UITableViewDelegate{
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 130
    }
}