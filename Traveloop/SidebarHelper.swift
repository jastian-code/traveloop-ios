//
//  SidebarHelper.swift
//  Traveloop
//
//  Created by Apple on 9/29/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class SidebarHelper : UIViewController {
    class var sharedInstance : SidebarHelper {
        struct Singleton {
            static let instance = SidebarHelper()
        }
        
        return Singleton.instance
    }
    
    func checkRevealViewController(menuButton : UIBarButtonItem!) {
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            let screenSize = UIScreen.mainScreen().bounds.size.width
            self.revealViewController().rearViewRevealWidth = (screenSize * 80) / 100
        }

    }
    
}