//
//  AirlineFlight.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/1/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

struct AirlineFlight {
    var originCityCode: String = ""
    var originAirportCode: String = ""
    var departureTime: String = ""
    var departureDate: String = ""
    
    var arrivalCityCode: String = ""
    var arrivalAirportCode: String = ""
    var arrivalTime: String = ""
    var arrivalDate: String = ""
    
    var airlineNo: String = ""
    var airlineCode: String = ""
    var airlineName: String = ""
    var flightCode: String = ""
    var bagage: Int = 0
    var availableSeatsValue : Int = 0
    
    var duration: String = ""
    
    var flightNumber: String {get{
            return "\(airlineCode)-\(flightCode)"
        }
    }
    var arrivalTimeDisplay: String{ get{
        return timeDisplay(arrivalTime)
    }}
    var departureTimeDisplay: String{ get{
        return timeDisplay(departureTime)
    }}
    
    private func timeDisplay(target: String) -> String{
        var result = target
        
        if let sepIdx = target.characters.indexOf("-"){
            result = target.substringToIndex(sepIdx)
        }
        else if let sepIdx = target.characters.indexOf("+"){
            result = target.substringToIndex(sepIdx)
        }
        
        if result.contains(":") && result.length >= 5{
            result = result.substringWithRange(0, end: 5)
        }
        
        return result
    }
}




