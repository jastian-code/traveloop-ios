//
//  Carousel.swift
//  Traveloop
//
//  Created by apple on 10/1/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

struct carousel{
    var image : UIImage
    var title : String
    var url : NSURL
    
    init(image: UIImage, title: String, url: NSURL) {
        self.image = image
        self.title = title
        self.url = url
    }
} 


//init(title: String, artist: String, genre: String, coverUrl: String, year: String) {
//    super.init()
//    self.title = title
//    self.artist = artist
//    self.genre = genre
//    self.coverUrl = coverUrl
//    self.year = year
//}