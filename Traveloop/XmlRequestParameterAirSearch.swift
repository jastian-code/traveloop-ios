//
//  XmlRequestParameterAirSearch.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/25/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class XmlRequestParameterAirSearch : XmlRequestParameterBase {
    var adultPassengerCount: Int = 0
    var childPassengerCount: Int = 0
    var infantPassengerCount: Int = 0
    var originAirportCode: String = ""
    var originCity: String = ""
    var originDisplay: String = ""
    var destinationAirportCode: String = ""
    var destinationCity: String = ""
    var destinationDisplay: String = ""
    var isRoundTrip: Bool = false{
        didSet{
            self.xmlFileName = isRoundTrip ? "FlightSearchXmlRequestRoundTrip" : "FlightSearchXmlRequest"
        }
    }
    var departureDate : NSDate?
    var departureDateString: String = ""
    var departureDateDisplay: String = ""
    var returnDate: NSDate?
    var returnDateString: String = ""
    var returnDateDisplay: String = ""
    var responseType : APIResponseType = APIResponseType.json{
        didSet{
            switch self.responseType  {
                case APIResponseType.json: self.apiURI = TraveloopAPIUri.flightSearch;break;
                case APIResponseType.xml: self.apiURI = TraveloopAPIUri.flightSearch + ".xml";break;
            }
        }
    }
    
    init(){
        super.init(type: APIServiceType.AirSearch, HTTPMethod: HTTPMethodType.POST)
        
        self.apiURI = TraveloopAPIUri.flightSearch
        self.xmlFileName = "FlightSearchXmlRequest"
        
        self.headerParam = ["B2B-Action" : "AirSearch"]
    }
    
    
    override func replaceXmlValue(var xml: NSString)-> NSString{
        xml = xml.stringByReplacingOccurrencesOfString("{DepartureDate}", withString: self.departureDateString)
        xml = xml.stringByReplacingOccurrencesOfString("{ReturnDate}", withString: self.returnDateString)
        xml = xml.stringByReplacingOccurrencesOfString("{Origin}", withString: self.originAirportCode)
        xml = xml.stringByReplacingOccurrencesOfString("{Destination}", withString: self.destinationAirportCode)
        xml = xml.stringByReplacingOccurrencesOfString("{RoundTrip}", withString: self.isRoundTrip ? "Round Trip" : "One Way")
        xml = xml.stringByReplacingOccurrencesOfString("{AdultPassenger}", withString: "\(self.adultPassengerCount)")
        xml = xml.stringByReplacingOccurrencesOfString("{ChildPassenger}", withString: "\(self.childPassengerCount)")
        xml = xml.stringByReplacingOccurrencesOfString("{InfantPassenger}", withString: "\(self.infantPassengerCount)")
        
        return xml
    }
    
}
