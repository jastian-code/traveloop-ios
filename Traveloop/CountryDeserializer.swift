//
//  CountryDeserializer.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

class CountryDeserializer : CoreDataDeserializable{
    var apiUrl = TraveloopAPIUri.getCountries
    
    func deserialize(context: NSManagedObjectContext,  json : JSON) -> [NSManagedObject]{
        var countries = [Country]()
        let jsonResult = json["Countries"]
        
        for(_, subJson) : (String, JSON) in jsonResult{
            
            let id = subJson["id"].intValue
            
            var country = Country.findById(context, id: id)
            if country == nil{
                country = Country.create(context)
                country!.id = id
            }
            
            country!.countryName = subJson["countryName"].stringValue
            country!.countryIATACode = subJson["countryIATACode"].stringValue
            country!.countryISO2CharCode = subJson["countryISO2CharCode"].stringValue
            country!.countryISO3CharCode = subJson["countryISO3CharCode"].stringValue
            country!.countryISONumberCode = subJson["countryISONumberCode"].stringValue
            country!.isActive = subJson["isActive"].boolValue
            
            if let createdDate = subJson["createdDate"].stringValue as? String{
                country!.createdDate = DateUtility.dateFromString(createdDate, format: "yyyy/MM/dd")
            }
            if let modifiedDate = subJson["modifiedDate"].stringValue as? String{
                country!.modifiedDate = DateUtility.dateFromString(modifiedDate, format: "yyyy/MM/dd")
            }
            
            countries.append(country!)
        }
        
        
        return countries
        
    }
}