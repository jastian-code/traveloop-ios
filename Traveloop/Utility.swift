//
//  Utility.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/14/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class Utility{
    class func classNameAsString(obj: Any) -> String {
        return _stdlib_getDemangledTypeName(obj).componentsSeparatedByString(".")[1]
    }
}