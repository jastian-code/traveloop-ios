//
//  FlightSearchResultViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/30/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class FlightSearchResultViewControllerOld: UITableViewController {
    
    @IBOutlet weak var viewLoadingScreen: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelMessage: UILabel!
    
    var pageIdx = 0 //0 = FlightSearchResultDepart 1= FlightSearchResultReturn
    var requestParam: XmlRequestParameterAirSearch?
    var flightSearchResultsToBind = [FlightSearchResult]()
    var flightSearchResults = [FlightSearchResult]()
    var flightSearchResultsReturn = [FlightSearchResult]()
    var languageDict : [String: String] = [:]
    
    
    @IBAction func backButtonItemTouchUp(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if(pageIdx == 0){
            languageDict = GlobalFunction.getLanguageDict()
            
            self.viewLoadingScreen.hidden = false
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            self.labelMessage.hidden = true
            
            requestParam!.responseType = .json
            let factory = XmlRequestFactory(params: requestParam!)
            let request = factory.generate()
            
            //print("0 = \(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))")
            
            let operation = AFHTTPRequestOperation(request: request)
            operation.responseSerializer = AFJSONResponseSerializer()
            operation.setCompletionBlockWithSuccess({ (AFHTTPRequestOperation operation, AnyObject response) -> Void in
                //Response
                let json = JSON(response)
                
                if self.requestParam!.isRoundTrip{
                    var jsonFlightResult : JSON
                    
                    if json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][0] != nil{
                        
                        jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][0]["@val"]["FlightResults"]["@val"]["FlightResult"]
                        
                        let jsonFlightResultReturn = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][1]["@val"]["FlightResults"]["@val"]["FlightResult"]
                        self.flightSearchResultsReturn = self.bindResult(jsonFlightResultReturn)
                        
                    }else{
                        jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"]["@val"]["FlightResults"]["@val"]["FlightResult"]
                    }
                    
                    
                    self.flightSearchResults = self.bindResult(jsonFlightResult)
                }
                else{
                    let jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"]["@val"]["FlightResults"]["@val"]["FlightResult"]
                    
                    self.flightSearchResults = self.bindResult(jsonFlightResult)
                }
                
                
                self.flightSearchResultsToBind = self.flightSearchResults
                self.tableView.reloadData()
                
                self.activityIndicator.hidden = true
                self.activityIndicator.stopAnimating()
                if self.flightSearchResultsToBind.count > 0{
                    self.viewLoadingScreen.hidden = true
                }
                else{
                    self.labelMessage.text = self.languageDict["FlightsNotAvailable"]
                    self.viewLoadingScreen.hidden = false
                    self.labelMessage.hidden = false
                }
                
                
                
                }, failure: { (AFHTTPRequestOperation operation, NSError error) -> Void in
                    self.activityIndicator.hidden = true
                    self.activityIndicator.stopAnimating()
                    
                    self.viewLoadingScreen.hidden = false
                    self.labelMessage.hidden = false
                    
                    switch error.code{
                    case -1009: self.labelMessage.text = self.languageDict["Error-1009"];break;
                    default: self.labelMessage.text = error.localizedDescription; break;
                    }
                    
                    
            })
            operation.start()
            
            //call AirSearch API xml version to get flightResult to be pass to next step: AirDetailsUpdate API
            requestParam!.responseType = .xml
            let factory2 = XmlRequestFactory(params: requestParam!)
            let request2 = factory2.generate()
            let operation2 = AFHTTPRequestOperation(request: request2)
            operation2.responseSerializer = AFXMLParserResponseSerializer()
            operation2.setCompletionBlockWithSuccess({ (AFHTTPRequestOperation operation, AnyObject response) -> Void in
                
                do{
                    let xmlDoc = try AEXMLDocument(xmlData: operation2.responseData!)
                    print (xmlDoc.root["status"].value)
                    
                }catch{
                    print(error)
                }
                
                //print(operation.responseString)
                let xmlParser = response as! NSXMLParser
                
                xmlParser.delegate = self
                //xmlParser.parse()
                
                
                }, failure: { (AFHTTPRequestOperation operation, NSError error) -> Void in
                    
                    
                    switch error.code{
                    case -1009: print(self.languageDict["Error-1009"]);break;
                    default: print(error.localizedDescription); break;
                    }
                    
                    
            })
            operation2.start()
            
        }
        else{
            let label = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 44))
            label.backgroundColor = UIColor.clearColor()
            label.textColor = UIColor.whiteColor()
            label.numberOfLines = 2
            label.font = UIFont(name: label.font.fontName, size: 12)
            label.textAlignment = NSTextAlignment.Left
            label.text = "Return Flight - \(self.requestParam!.returnDateDisplay)\n\(self.requestParam!.destinationDisplay) - \(self.requestParam!.originDisplay) "
            
            self.navigationItem.titleView = label
            
            self.flightSearchResultsToBind = self.flightSearchResultsReturn
            self.tableView.reloadData()
            
            self.activityIndicator.hidden = true
            self.activityIndicator.stopAnimating()
            if self.flightSearchResultsToBind.count > 0{
                self.viewLoadingScreen.hidden = true
            }
            else{
                self.labelMessage.text = self.languageDict["FlightsNotAvailable"]
                self.viewLoadingScreen.hidden = false
                self.labelMessage.hidden = false
            }
        }
        
    }
    
    
    private func bindResult(jsonFlightResult: JSON) -> [FlightSearchResult]{
        
        var listResult = [FlightSearchResult]()
        
        
        if(jsonFlightResult != nil){
            //check if jsonFlightResult[0] is nil, indicates that only return single object result (not an array)
            if(jsonFlightResult[0] == nil){
                var flightSearchResult = FlightSearchResult()
                
                flightSearchResult.id = jsonFlightResult["@attr"]["FlightResultID"].string!
                flightSearchResult.stopsNum = jsonFlightResult["@attr"]["StopsNum"].intValue
                flightSearchResult.allowRefund = jsonFlightResult["@attr"]["RefundableFareIndicator"].boolValue
                flightSearchResult.currencyCode = jsonFlightResult["@val"]["Fare"]["@val"]["Currency"]["@attr"]["CurrencyCode"].stringValue
                flightSearchResult.totalFareAmt = jsonFlightResult["@val"]["Fare"]["@attr"]["TotalFareAmt"].doubleValue
                flightSearchResult.origTotalFareAmt = jsonFlightResult["@val"]["Fare"]["@attr"]["OrigTotalFareAmt"].doubleValue
                flightSearchResult.totalServiceCharges = jsonFlightResult["@val"]["Fare"]["@attr"]["TotalServiceCharges"].doubleValue
                
                let jsonFlightSegment = jsonFlightResult["@val"]["FlightSegments"]["@val"]["FlightSegment"]
                
                bindDetail(&flightSearchResult, jsonFlightSegment: jsonFlightSegment)
                
                listResult.append(flightSearchResult)
                
            }else{
                
                for(_, subJson) : (String, JSON) in jsonFlightResult{
                    
                    var flightSearchResult = FlightSearchResult()
                    
                    flightSearchResult.id = subJson["@attr"]["FlightResultID"].string!
                    flightSearchResult.stopsNum = subJson["@attr"]["StopsNum"].intValue
                    flightSearchResult.allowRefund = subJson["@attr"]["RefundableFareIndicator"].boolValue
                    flightSearchResult.currencyCode = subJson["@val"]["Fare"]["@val"]["Currency"]["@attr"]["CurrencyCode"].stringValue
                    flightSearchResult.totalFareAmt = subJson["@val"]["Fare"]["@attr"]["TotalFareAmt"].doubleValue
                    flightSearchResult.origTotalFareAmt = subJson["@val"]["Fare"]["@attr"]["OrigTotalFareAmt"].doubleValue
                    flightSearchResult.totalServiceCharges = subJson["@val"]["Fare"]["@attr"]["TotalServiceCharges"].doubleValue
                    
                    let jsonFlightSegment = subJson["@val"]["FlightSegments"]["@val"]["FlightSegment"]
                    
                    bindDetail(&flightSearchResult, jsonFlightSegment: jsonFlightSegment)
                    
                    listResult.append(flightSearchResult)
                    
                    
                }
            }
        }
        
        return listResult
    }
    
    private func bindDetail(inout flightSearchResult: FlightSearchResult, jsonFlightSegment: JSON){
        
        for(innerKey, innerSubJson) : (String, JSON) in jsonFlightSegment{
            
            var flight = AirlineFlight()
            
            //check if innerKey return integer then Segment is an array else segment is not array
            var innerSubJsonRoot : JSON
            if(Int(innerKey) != nil ){
                innerSubJsonRoot = innerSubJson["@val"]
            }
            else if innerKey == "@val"{
                innerSubJsonRoot = innerSubJson
            }
            else{
                continue
            }
            
            
            flight.originCityCode = innerSubJsonRoot["OriginLocation"]["@attr"]["CityCode"].stringValue
            flight.originAirportCode = innerSubJsonRoot["OriginLocation"]["@attr"]["AirportCode"].stringValue
            flight.departureDate = innerSubJsonRoot["OriginLocation"]["@attr"]["DepartureDate"].stringValue
            flight.departureTime = innerSubJsonRoot["OriginLocation"]["@attr"]["DepartureTime"].stringValue
            
            flight.arrivalCityCode = innerSubJsonRoot["DestinationLocation"]["@attr"]["CityCode"].stringValue
            flight.arrivalAirportCode = innerSubJsonRoot["DestinationLocation"]["@attr"]["AirportCode"].stringValue
            flight.arrivalDate = innerSubJsonRoot["DestinationLocation"]["@attr"]["ArrivalDate"].stringValue
            flight.arrivalTime = innerSubJsonRoot["DestinationLocation"]["@attr"]["ArrivalTime"].stringValue
            
            flight.airlineNo = innerSubJsonRoot["FlightDetails"]["@attr"]["AirlineNo"].stringValue
            flight.airlineCode = innerSubJsonRoot["FlightDetails"]["@attr"]["OperatingAirlineCode"].stringValue
            flight.airlineName = innerSubJsonRoot["FlightDetails"]["@attr"]["AirlineName"].stringValue
            flight.flightCode = innerSubJsonRoot["FlightDetails"]["@attr"]["FlightCode"].stringValue
            flight.availableSeatsValue = innerSubJsonRoot["FlightDetails"]["@attr"]["AvailableSeatsValue"].intValue
            
            flightSearchResult.flightSegments.append(flight)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.flightSearchResultsToBind.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
        
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("cellResult", forIndexPath: indexPath) as? MGSwipeTableCell
        if cell == nil
        {
            cell = MGSwipeTableCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cellResult")
        }
        //configure right buttons
        cell?.delegate = self
        
        
        let result = self.flightSearchResultsToBind[indexPath.row]
        
        let lblAirline = view.viewWithTag(1) as? UILabel
        let lblTime = view.viewWithTag(2) as? UILabel
        let lblTransit = view.viewWithTag(3) as? UILabel
        let lblRefundable = view.viewWithTag(4) as? UILabel
        let lblPrice = view.viewWithTag(5) as? UILabel
        
        
        if result.flightSegments.count > 0{
            if let segment = result.flightSegments[0] as? AirlineFlight{
                
                lblAirline?.text = segment.airlineName
                lblTime?.text = "\(segment.departureTimeDisplay) - \(result.flightSegments[result.flightSegments.count - 1].arrivalTimeDisplay)"
                lblTransit?.text = result.stopsNum == 0 ? languageDict["Direct"]! : (result.stopsNum == 1 ? "1 \(languageDict["Transit"]!)" : " \(result.stopsNum) \(languageDict["Transits"]!)")
                
                
                lblRefundable?.text = result.allowRefund ? languageDict["Refundable"]! : languageDict["NotRefundable"]

                
                lblPrice?.text = "\(result.currencyCode) \(AmountFormatter.toString(result.origTotalFareAmt))"
            }
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(self.pageIdx == 0 && self.requestParam!.isRoundTrip){
            
            
            if let viewFlightResult = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightResultUserControllerIdentifier) as? FlightSearchResultViewController{
                
                
                viewFlightResult.pageIdx = 1
                viewFlightResult.flightSearchResults = self.flightSearchResults
                viewFlightResult.flightSearchResultsReturn = self.flightSearchResultsReturn
                viewFlightResult.flightSearchResultsToBind = self.flightSearchResultsToBind
                viewFlightResult.requestParam = self.requestParam
                viewFlightResult.languageDict = languageDict
                
                self.navigationController?.pushViewController(viewFlightResult, animated: true)
                
                print(self.flightSearchResultsToBind[indexPath.row].id)
            }
        }
        else{
            print(self.flightSearchResultsToBind[indexPath.row].id)
        }
    }
    
}


extension FlightSearchResultViewControllerOld : MGSwipeTableCellDelegate{
    
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        
        if(direction == .RightToLeft){
            cell!.rightSwipeSettings.transition = MGSwipeTransition.Drag
            let btnDetails = MGSwipeButton(title: "Details", backgroundColor: UIColor.grayColor(),
                callback:{ (sender:MGSwipeTableCell!) -> Bool in
                    
                    let viewDetails = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightDetailsUserControllerIdentifier) as! FlightDetailsViewController
                    let navDetails = UINavigationController(rootViewController: viewDetails)
                    
                    
                    let selectedIdxPath : NSIndexPath = self.tableView.indexPathForCell(cell)!
                    viewDetails.flight = self.flightSearchResultsToBind[selectedIdxPath.row]
                    viewDetails.title = self.pageIdx == 0 ? "Departure Flight Details" : "Return Flight Details"
                    
                    self.navigationController?.presentViewController(navDetails, animated: true, completion: nil)
                    
                    return true
            })
            btnDetails.titleLabel?.font = UIFont(name: btnDetails.titleLabel!.font.fontName, size: 14)
            cell!.rightButtons = [btnDetails]
            
            return [btnDetails]
        }
        return nil
        
    }
    
}

extension FlightSearchResultViewControllerOld : NSXMLParserDelegate{
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        print("Element's name is \(elementName)")
        print("Element's attributes are \(attributeDict)")
        
        if elementName == "FlightResults"{
            //print(String(parser))
            
        }
    }
    func parser(parser: NSXMLParser!, foundCharacters string: String!) {
        //append characters to self.elementValue
        //print( string)
    }
}

