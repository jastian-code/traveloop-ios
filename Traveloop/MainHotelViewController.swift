//
//  MainHotelViewController.swift
//  Traveloop
//
//  Created by apple on 9/30/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class MainHotelViewController: UIViewController {

    let cellIdentifier = ["cellCity", "cellCheckin", "cellCheckout","cellSearch","cellEmpty"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
 
}

extension MainHotelViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellIdentifier.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier[indexPath.row], forIndexPath: indexPath) as? UITableViewCell
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let defaultHeight = tableView.rowHeight
        switch indexPath.row {
            case cellIdentifier.indexOf("cellSearch")!:
                return 60
            default:
                return defaultHeight
        }

    }
}

extension MainHotelViewController: UITableViewDelegate {
    
}
