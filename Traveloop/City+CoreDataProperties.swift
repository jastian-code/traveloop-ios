//
//  City+CoreDataProperties.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension City {

    @NSManaged var cityName: String?
    @NSManaged var createdDate: NSDate?
    @NSManaged var id: NSNumber?
    @NSManaged var modifiedDate: NSDate?
    @NSManaged var isActive: NSNumber?
    @NSManaged var airports: NSOrderedSet?
    @NSManaged var country: Country?

}
