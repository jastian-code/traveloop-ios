//
//  FlightSearchResultViewController.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/30/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class FlightSearchResultViewController: UITableViewController {
  
    @IBOutlet weak var viewLoadingScreen: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var labelMessage: UILabel!
    
    var pageIdx = 0 //0 = FlightSearchResultDepart 1= FlightSearchResultReturn
    var requestParam: XmlRequestParameterAirSearch?
    var flightSearchResultsToBind = [FlightSearchResult]()
    var flightSearchResults = [FlightSearchResult]()
    var flightSearchResultsReturn = [FlightSearchResult]()
    var languageDict : [String: String] = [:]
    
    var selectedFlightSearchResult: FlightSearchResult?
    var selectedFlightSearchResultXmlString: String = ""
    
    var flightResultXmlStringDict: [String: String] = [:]
    var flightResultReturnXmlStringDict: [String: String] = [:]
    
    @IBAction func backButtonItemTouchUp(sender: AnyObject) {
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageDict = GlobalFunction.getLanguageDict()
        
        self.title = self.languageDict["Details"]
        
        if(pageIdx == 0){
            
            self.viewLoadingScreen.hidden = false
            self.activityIndicator.hidden = false
            self.activityIndicator.startAnimating()
            self.labelMessage.hidden = true
            
            requestParam!.responseType = .xml
            let factory = XmlRequestFactory(params: requestParam!)
            let request = factory.generate()
            
            //print("0 = \(NSString(data: request.HTTPBody!, encoding: NSUTF8StringEncoding))")
            
            let operation = AFHTTPRequestOperation(request: request)
            operation.responseSerializer = AFXMLParserResponseSerializer()
            operation.setCompletionBlockWithSuccess({ (AFHTTPRequestOperation operation, AnyObject response) -> Void in
                
                self.processFromXml(operation, response: response)
                
                self.flightSearchResultsToBind = self.flightSearchResults
                self.tableView.reloadData()
                
                self.activityIndicator.hidden = true
                self.activityIndicator.stopAnimating()
                if self.flightSearchResultsToBind.count > 0{
                    self.viewLoadingScreen.hidden = true
                }
                else{
                    self.labelMessage.text = self.languageDict["FlightsNotAvailable"]
                    self.viewLoadingScreen.hidden = false
                    self.labelMessage.hidden = false
                }
                
                
                
                }, failure: { (AFHTTPRequestOperation operation, NSError error) -> Void in
                    self.activityIndicator.hidden = true
                    self.activityIndicator.stopAnimating()
                    
                    self.viewLoadingScreen.hidden = false
                    self.labelMessage.hidden = false
                    
                    switch error.code{
                    case -1009: self.labelMessage.text = self.languageDict["Error-1009"];break;
                    default: self.labelMessage.text = error.localizedDescription; break;
                    }
                    
                    
            })
            operation.start()
            
        }
        else{
            let label = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.width, 44))
            label.backgroundColor = UIColor.clearColor()
            label.textColor = UIColor.whiteColor()
            label.numberOfLines = 2
            label.font = UIFont(name: label.font.fontName, size: 11)
            label.textAlignment = NSTextAlignment.Left
            label.text = "\(self.languageDict["ReturnFlight"]!) - \(self.requestParam!.returnDateDisplay)\n\(self.requestParam!.destinationDisplay) - \(self.requestParam!.originDisplay) "
            
            self.navigationItem.titleView = label
            
            self.flightSearchResultsToBind = self.flightSearchResultsReturn
            self.tableView.reloadData()
            
            self.activityIndicator.hidden = true
            self.activityIndicator.stopAnimating()
            if self.flightSearchResultsToBind.count > 0{
                self.viewLoadingScreen.hidden = true
            }
            else{
                self.labelMessage.text = self.languageDict["FlightsNotAvailable"]
                self.viewLoadingScreen.hidden = false
                self.labelMessage.hidden = false
            }
        }
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

// MARK: - Table view data source
extension FlightSearchResultViewController{
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.flightSearchResultsToBind.count
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 65
        
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell = self.tableView.dequeueReusableCellWithIdentifier("cellResult", forIndexPath: indexPath) as? MGSwipeTableCell
        if cell == nil
        {
            cell = MGSwipeTableCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: "cellResult")
        }
        //configure right buttons
        cell?.delegate = self
        
        
        let result = self.flightSearchResultsToBind[indexPath.row]
        
        let lblAirline = view.viewWithTag(1) as? UILabel
        let lblTime = view.viewWithTag(2) as? UILabel
        let lblTransit = view.viewWithTag(3) as? UILabel
        let lblRefundable = view.viewWithTag(4) as? UILabel
        let lblPrice = view.viewWithTag(5) as? UILabel
        
        
        if result.flightSegments.count > 0{
            if let segment = result.flightSegments[0] as? AirlineFlight{
                
                lblAirline?.text = segment.airlineName
                lblTime?.text = "\(segment.departureTimeDisplay) - \(result.flightSegments[result.flightSegments.count - 1].arrivalTimeDisplay)"
                lblTransit?.text = result.stopsNum == 0 ? languageDict["Direct"]! : (result.stopsNum == 1 ? "1 \(languageDict["Transit"]!)" : " \(result.stopsNum) \(languageDict["Transits"]!)")

                
                lblRefundable?.text = result.allowRefund ? languageDict["Refundable"]! : languageDict["NotRefundable"]
                lblPrice?.text = "\(result.currencyCode) \(AmountFormatter.toString(result.origTotalFareAmt))"
                
                let imgViewAirline = view.viewWithTag(6) as? UIImageView
                imgViewAirline?.contentMode = UIViewContentMode.ScaleAspectFit
                
                if kAirlineImageIcon[segment.airlineName] != nil{
                    if let img = UIImage(named:  kAirlineImageIcon[(segment.airlineName)]!){
                        let imgAirline = ImageHelper.resizeImage(img, newHeight: 20)
                        imgViewAirline?.frame = CGRect(x: imgViewAirline!.frame.origin.x, y: imgViewAirline!.frame.origin.y, width: imgAirline.size.width, height: imgAirline.size.height)
                        imgViewAirline?.image = imgAirline
                    }
                }
            }
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(self.pageIdx == 0 && self.requestParam!.isRoundTrip){
            
            
            if let viewFlightResult = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightResultUserControllerIdentifier) as? FlightSearchResultViewController{
                
                self.selectedFlightSearchResult = self.flightSearchResultsToBind[indexPath.row]
                self.selectedFlightSearchResultXmlString = self.flightResultXmlStringDict[String(indexPath.row)] ?? ""
                
                viewFlightResult.pageIdx = 1
                viewFlightResult.flightSearchResults = self.flightSearchResults
                viewFlightResult.flightSearchResultsReturn = self.flightSearchResultsReturn
                viewFlightResult.flightSearchResultsToBind = self.flightSearchResultsToBind
                viewFlightResult.requestParam = self.requestParam
                viewFlightResult.languageDict = languageDict
                viewFlightResult.flightResultXmlStringDict = self.flightResultXmlStringDict
                viewFlightResult.flightResultReturnXmlStringDict = self.flightResultReturnXmlStringDict
                viewFlightResult.selectedFlightSearchResult = self.selectedFlightSearchResult
                viewFlightResult.selectedFlightSearchResultXmlString = self.selectedFlightSearchResultXmlString
                
                self.navigationController?.pushViewController(viewFlightResult, animated: true)
            }
        }
        else{
           
            if let viewbookProcess = self.storyboard?.instantiateViewControllerWithIdentifier (kFlightBookProcessUserControllerIdentifier) as? FlightBookProcessViewController{
           
                
                if self.requestParam!.isRoundTrip{
                    viewbookProcess.requestParam.flight = self.selectedFlightSearchResult ?? self.flightSearchResultsToBind[indexPath.row]
                    viewbookProcess.requestParam.flightReturn = self.flightSearchResultsToBind[indexPath.row]
                    
                    viewbookProcess.requestParam.airSearchFlightDetailXmlResponse = self.selectedFlightSearchResultXmlString
                    viewbookProcess.requestParam.airSearchReturnFlightDetailXmlResponse = self.flightResultReturnXmlStringDict[String(indexPath.row)] ?? ""
                }else{
                    viewbookProcess.requestParam.flight = self.flightSearchResultsToBind[indexPath.row]
                    viewbookProcess.requestParam.airSearchFlightDetailXmlResponse = self.flightResultXmlStringDict[String(indexPath.row)] ?? ""
                }
                
                viewbookProcess.requestParam.adultPassengerCount = self.requestParam!.adultPassengerCount
                viewbookProcess.requestParam.childPassengerCount = self.requestParam!.childPassengerCount
                viewbookProcess.requestParam.infantPassengerCount = self.requestParam!.infantPassengerCount
                viewbookProcess.requestParam.originAirportCode = self.requestParam!.originAirportCode
                viewbookProcess.requestParam.originCity = self.requestParam!.originCity
                viewbookProcess.requestParam.originDisplay = self.requestParam!.originDisplay
                viewbookProcess.requestParam.destinationAirportCode = self.requestParam!.destinationAirportCode
                viewbookProcess.requestParam.destinationCity = self.requestParam!.destinationCity
                viewbookProcess.requestParam.destinationDisplay = self.requestParam!.destinationDisplay
                viewbookProcess.requestParam.isRoundTrip = self.requestParam!.isRoundTrip
                viewbookProcess.requestParam.departureDate = self.requestParam!.departureDate
                viewbookProcess.requestParam.departureDateString = self.requestParam!.departureDateString
                viewbookProcess.requestParam.departureDateDisplay = self.requestParam!.departureDateDisplay
                viewbookProcess.requestParam.returnDate = self.requestParam!.returnDate
                viewbookProcess.requestParam.returnDateString = self.requestParam!.returnDateString
                viewbookProcess.requestParam.returnDateDisplay = self.requestParam!.returnDateDisplay
                viewbookProcess.requestParam.responseType = .xml
                
                
                self.navigationController?.pushViewController(viewbookProcess, animated: true)
            }
            
        }
    }
}

extension FlightSearchResultViewController : MGSwipeTableCellDelegate{
    
    func swipeTableCell(cell: MGSwipeTableCell!, swipeButtonsForDirection direction: MGSwipeDirection, swipeSettings: MGSwipeSettings!, expansionSettings: MGSwipeExpansionSettings!) -> [AnyObject]! {
        
        
        if(direction == .RightToLeft){
            cell!.rightSwipeSettings.transition = MGSwipeTransition.Drag
            let btnDetails = MGSwipeButton(title: self.languageDict["Details"], backgroundColor: UIColor.grayColor(),
            callback:{ (sender:MGSwipeTableCell!) -> Bool in
                
                let viewDetails = self.storyboard?.instantiateViewControllerWithIdentifier(kFlightDetailsUserControllerIdentifier) as! FlightDetailsViewController
                let navDetails = UINavigationController(rootViewController: viewDetails)
                
                
                let selectedIdxPath : NSIndexPath = self.tableView.indexPathForCell(cell)!
                viewDetails.flight = self.flightSearchResultsToBind[selectedIdxPath.row]
                viewDetails.title = self.pageIdx == 0 ? self.languageDict["DepartureFlightDetails"] : self.languageDict["ReturnFlightDetails"]
                
                self.navigationController?.presentViewController(navDetails, animated: true, completion: nil)
                
                return true
            })
            btnDetails.titleLabel?.font = UIFont(name: btnDetails.titleLabel!.font.fontName, size: 14)
            cell!.rightButtons = [btnDetails]
        
            return [btnDetails]
        }
        return nil
        
    }
    
}

extension FlightSearchResultViewController : NSXMLParserDelegate{
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        //print("Element's name is \(elementName)")
        //print("Element's attributes are \(attributeDict)")
        
    }
    func parser(parser: NSXMLParser!, foundCharacters string: String!) {
        //append characters to self.elementValue
        //print( string)
    }
}



//Handle Xml Response
extension FlightSearchResultViewController{
    
    
    private func processFromXml(operation : AFHTTPRequestOperation , response : AnyObject){
        do{
            let xmlDoc = try AEXMLDocument(xmlData: operation.responseData!)
            
            let xmlDocDepart = xmlDoc.root["data"]["ONTRA_AirSearchRS"]["OriginDestinationOptions"]["OriginDestinationOption"].first
            self.flightResultXmlStringDict.removeAll()
            self.flightSearchResults = self.bindResult(xmlDocDepart, flightResultDict: &self.flightResultXmlStringDict)
            
            if self.requestParam!.isRoundTrip{
                let xmlDocReturn = xmlDoc.root["data"]["ONTRA_AirSearchRS"]["OriginDestinationOptions"]["OriginDestinationOption"].last
                
                self.flightResultReturnXmlStringDict.removeAll()
                self.flightSearchResultsReturn = self.bindResult(xmlDocReturn, flightResultDict: &self.flightResultReturnXmlStringDict)
            }
            
        }catch{
            print(error)
        }
    }
    
    private func bindResult(xmlDocFlightResult: AEXMLElement?, inout flightResultDict: [String: String]) -> [FlightSearchResult]{
        
   
        var listResult = [FlightSearchResult]()
        
        if(xmlDocFlightResult != nil){
            if(xmlDocFlightResult!.children.first?.count > 0){
                for flightResult in (xmlDocFlightResult!.children.first?.children)!{
                    
                    let flightResultId = flightResult.attributes["FlightResultID"]!
                    flightResultDict[flightResultId] = flightResult.xmlString
                    
                    var flightSearchResult = FlightSearchResult()
                    
                    flightSearchResult.id = flightResultId
                    flightSearchResult.stopsNum = Int(flightResult.attributes["StopsNum"] ?? "0")!
                    flightSearchResult.totalJourneyDuration = Int(flightResult.attributes["TotalJourneyDuration"] ?? "0")!
                    flightSearchResult.allowRefund = (flightResult.attributes["RefundableFareIndicator"] ?? "false").toBool()!
                    let currency = flightResult.children.lazy.filter{$0.name == "Fare"}.first?.children.lazy.filter{$0.name == "Currency"}.first
                    if currency != nil{
                        flightSearchResult.currencyCode = currency!.attributes["CurrencyCode"]!
                    }
                    let fare = flightResult.children.lazy.filter{$0.name == "Fare"}.first
                    if fare != nil{
                        flightSearchResult.totalFareAmt = Double(fare!.attributes["TotalFareAmt"] ?? "0")!
                        flightSearchResult.origTotalFareAmt = Double(fare!.attributes["OrigTotalFareAmt"] ?? "0")!
                        flightSearchResult.totalServiceCharges = Double(fare!.attributes["TotalServiceCharges"] ?? "0")!
                    }
                    
                    //bind details: flightSegment/flight transits
                    let flightSegments = flightResult.children.lazy.filter{$0.name == "FlightSegments"}.first?.children.lazy.filter{$0.name == "FlightSegment"}
                    if flightSegments != nil{
                        for flightSegment in flightSegments!{
                            var flight = AirlineFlight()
                            
                            let originLocation = flightSegment.children.lazy.filter{$0.name == "OriginLocation"}.first
                            if originLocation != nil{
                                flight.originCityCode = originLocation!.attributes["CityCode"]!
                                flight.originAirportCode = originLocation!.attributes["AirportCode"]!
                                flight.departureDate = originLocation!.attributes["DepartureDate"]!
                                flight.departureTime = originLocation!.attributes["DepartureTime"]!
                            }
                            
                            let destinationLocation = flightSegment.children.lazy.filter{$0.name == "DestinationLocation"}.first
                            if destinationLocation != nil{
                                flight.arrivalCityCode = destinationLocation!.attributes["CityCode"]!
                                flight.arrivalAirportCode = destinationLocation!.attributes["AirportCode"]!
                                flight.arrivalDate = destinationLocation!.attributes["ArrivalDate"]!
                                flight.arrivalTime = destinationLocation!.attributes["ArrivalTime"]!
                            }
                            
                            let flightDetails = flightSegment.children.lazy.filter{$0.name == "FlightDetails"}.first
                            if flightDetails != nil{
                                flight.airlineNo = flightDetails!.attributes["AirlineNo"]!
                                flight.airlineCode = flightDetails!.attributes["OperatingAirlineCode"]!
                                flight.airlineName = flightDetails!.attributes["AirlineName"]!
                                flight.flightCode = flightDetails!.attributes["FlightCode"]!
                                flight.availableSeatsValue = Int(flightDetails!.attributes["AvailableSeatsValue"] ?? "0")!
                            }
                            
                            flightSearchResult.flightSegments.append(flight)
                        }
                    }
                    
                    listResult.append(flightSearchResult)
                }
            }
        }
        
        return listResult
    }
    
}

//Handle Json Response
extension FlightSearchResultViewController{
    private func processFromJson(operation : AFHTTPRequestOperation , response : AnyObject){
        let json = JSON(response)
        
        if self.requestParam!.isRoundTrip{
            var jsonFlightResult : JSON
            
            if json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][0] != nil{
                
                jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][0]["@val"]["FlightResults"]["@val"]["FlightResult"]
                
                let jsonFlightResultReturn = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"][1]["@val"]["FlightResults"]["@val"]["FlightResult"]
                self.flightSearchResultsReturn = self.bindResult(jsonFlightResultReturn)
                
            }else{
                jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"]["@val"]["FlightResults"]["@val"]["FlightResult"]
            }
            
            
            self.flightSearchResults = self.bindResult(jsonFlightResult)
        }
        else{
            let jsonFlightResult = json["data"]["ONTRA_AirSearchRS"]["@val"]["OriginDestinationOptions"]["@val"]["OriginDestinationOption"]["@val"]["FlightResults"]["@val"]["FlightResult"]
            
            self.flightSearchResults = self.bindResult(jsonFlightResult)
        }
    }
    
    private func bindResult(jsonFlightResult: JSON) -> [FlightSearchResult]{
        
        var listResult = [FlightSearchResult]()
        
        
        if(jsonFlightResult != nil){
            //check if jsonFlightResult[0] is nil, indicates that only return single object result (not an array)
            if(jsonFlightResult[0] == nil){
                var flightSearchResult = FlightSearchResult()
                
                flightSearchResult.id = jsonFlightResult["@attr"]["FlightResultID"].string!
                flightSearchResult.stopsNum = jsonFlightResult["@attr"]["StopsNum"].intValue
                flightSearchResult.totalJourneyDuration = jsonFlightResult["@attr"]["TotalJourneyDuration"].intValue
                flightSearchResult.allowRefund = jsonFlightResult["@attr"]["RefundableFareIndicator"].boolValue
                flightSearchResult.currencyCode = jsonFlightResult["@val"]["Fare"]["@val"]["Currency"]["@attr"]["CurrencyCode"].stringValue
                flightSearchResult.totalFareAmt = jsonFlightResult["@val"]["Fare"]["@attr"]["TotalFareAmt"].doubleValue
                flightSearchResult.origTotalFareAmt = jsonFlightResult["@val"]["Fare"]["@attr"]["OrigTotalFareAmt"].doubleValue
                flightSearchResult.totalServiceCharges = jsonFlightResult["@val"]["Fare"]["@attr"]["TotalServiceCharges"].doubleValue
                
                let jsonFlightSegment = jsonFlightResult["@val"]["FlightSegments"]["@val"]["FlightSegment"]
                
                bindDetail(&flightSearchResult, jsonFlightSegment: jsonFlightSegment)
                
                listResult.append(flightSearchResult)
                
            }else{
                
                for(_, subJson) : (String, JSON) in jsonFlightResult{
                    
                    var flightSearchResult = FlightSearchResult()
                    
                    flightSearchResult.id = subJson["@attr"]["FlightResultID"].string!
                    flightSearchResult.stopsNum = subJson["@attr"]["StopsNum"].intValue
                    flightSearchResult.totalJourneyDuration = subJson["@attr"]["totalJourneyDuration"].intValue
                    flightSearchResult.allowRefund = subJson["@attr"]["RefundableFareIndicator"].boolValue
                    flightSearchResult.currencyCode = subJson["@val"]["Fare"]["@val"]["Currency"]["@attr"]["CurrencyCode"].stringValue
                    flightSearchResult.totalFareAmt = subJson["@val"]["Fare"]["@attr"]["TotalFareAmt"].doubleValue
                    flightSearchResult.origTotalFareAmt = subJson["@val"]["Fare"]["@attr"]["OrigTotalFareAmt"].doubleValue
                    flightSearchResult.totalServiceCharges = subJson["@val"]["Fare"]["@attr"]["TotalServiceCharges"].doubleValue
                    
                    let jsonFlightSegment = subJson["@val"]["FlightSegments"]["@val"]["FlightSegment"]
                    
                    bindDetail(&flightSearchResult, jsonFlightSegment: jsonFlightSegment)
                    
                    listResult.append(flightSearchResult)
                    
                    
                }
            }
        }
        
        return listResult
    }
    
    private func bindDetail(inout flightSearchResult: FlightSearchResult, jsonFlightSegment: JSON){
        
        for(innerKey, innerSubJson) : (String, JSON) in jsonFlightSegment{
            
            var flight = AirlineFlight()
            
            //check if innerKey return integer then Segment is an array else segment is not array
            var innerSubJsonRoot : JSON
            if(Int(innerKey) != nil ){
                innerSubJsonRoot = innerSubJson["@val"]
            }
            else if innerKey == "@val"{
                innerSubJsonRoot = innerSubJson
            }
            else{
                continue
            }
            
            
            flight.originCityCode = innerSubJsonRoot["OriginLocation"]["@attr"]["CityCode"].stringValue
            flight.originAirportCode = innerSubJsonRoot["OriginLocation"]["@attr"]["AirportCode"].stringValue
            flight.departureDate = innerSubJsonRoot["OriginLocation"]["@attr"]["DepartureDate"].stringValue
            flight.departureTime = innerSubJsonRoot["OriginLocation"]["@attr"]["DepartureTime"].stringValue
            
            flight.arrivalCityCode = innerSubJsonRoot["DestinationLocation"]["@attr"]["CityCode"].stringValue
            flight.arrivalAirportCode = innerSubJsonRoot["DestinationLocation"]["@attr"]["AirportCode"].stringValue
            flight.arrivalDate = innerSubJsonRoot["DestinationLocation"]["@attr"]["ArrivalDate"].stringValue
            flight.arrivalTime = innerSubJsonRoot["DestinationLocation"]["@attr"]["ArrivalTime"].stringValue
            
            flight.airlineNo = innerSubJsonRoot["FlightDetails"]["@attr"]["AirlineNo"].stringValue
            flight.airlineCode = innerSubJsonRoot["FlightDetails"]["@attr"]["OperatingAirlineCode"].stringValue
            flight.airlineName = innerSubJsonRoot["FlightDetails"]["@attr"]["AirlineName"].stringValue
            flight.flightCode = innerSubJsonRoot["FlightDetails"]["@attr"]["FlightCode"].stringValue
            flight.availableSeatsValue = innerSubJsonRoot["FlightDetails"]["@attr"]["AvailableSeatsValue"].intValue
            
            flightSearchResult.flightSegments.append(flight)
        }
    }
}
