//
//  Airport.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/14/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData


class Airport: NSManagedObject, CoreDataCRUD {
    static func findByCode(context: NSManagedObjectContext?, airportCode: String) -> Airport? {
        
        let fetchRequest = NSFetchRequest(entityName: Utility.classNameAsString(self))
        let predicate = NSPredicate(format: "airportCode == %@", airportCode)
        fetchRequest.predicate = predicate
        do{
            if let fetchResults = try context!.executeFetchRequest(fetchRequest) as? [Airport] {
                if fetchResults.count > 0 {
                    return fetchResults[0]
                }
            }
        }catch let err as NSError{
            print(err.localizedDescription)
        }
        return nil
    }
}
