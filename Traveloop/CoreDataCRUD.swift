//
//  CoreDataCRUD.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/14/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

protocol CoreDataCRUD {
    
}

extension CoreDataCRUD where Self : NSManagedObject {
    static func create(context: NSManagedObjectContext?) -> Self {

        return NSEntityDescription.insertNewObjectForEntityForName(Utility.classNameAsString(self), inManagedObjectContext: context!) as! Self
    }
    
    static func findById(context: NSManagedObjectContext?, id: Int) -> Self? {
        
        let fetchRequest = NSFetchRequest(entityName: Utility.classNameAsString(self))
        let predicate = NSPredicate(format: "id == %d", id)
        fetchRequest.predicate = predicate
        do{
            if let fetchResults = try context!.executeFetchRequest(fetchRequest) as? [Self] {
                if fetchResults.count > 0 {
                    return fetchResults[0]
                }
            }
        }catch let err as NSError{
            print(err.localizedDescription)
        }
        return nil
    }
    static func findById(context: NSManagedObjectContext?, id: String) -> Self? {
        
        let fetchRequest = NSFetchRequest(entityName: Utility.classNameAsString(self))
        let predicate = NSPredicate(format: "id == %@", id)
        fetchRequest.predicate = predicate
        do{
            if let fetchResults = try context!.executeFetchRequest(fetchRequest) as? [Self] {
                if fetchResults.count > 0 {
                    return fetchResults[0]
                }
            }
        }catch let err as NSError{
            print(err.localizedDescription)
        }
        return nil
    }
    static func delete(context: NSManagedObjectContext?, object: NSManagedObject) throws{
        context!.deleteObject(object)
    }
    
    static func getAll(var context: NSManagedObjectContext?) -> [Self]?{
        if context == nil{
            context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        }
        
        let request = NSFetchRequest(entityName: Utility.classNameAsString(self))
        request.returnsObjectsAsFaults = true
        
        do{
            let objects = try context!.executeFetchRequest(request) as? [Self]
            return objects
        }catch let err as NSError{
            print(err.localizedDescription)
        }
        return nil
    }
    
    static func count(context:NSManagedObjectContext) -> Int{
        let request = NSFetchRequest(entityName: Utility.classNameAsString(self))
        request.returnsObjectsAsFaults = true
        
        let error = NSErrorPointer()
        let count = context.countForFetchRequest(request, error: error)
        
        if error == nil{
            return count
        }else{
            print("Error: \(error.debugDescription)")
            return 0
        }
        
    }
    
    func save(context: NSManagedObjectContext) throws -> Bool{
        try context.save()
        return true
    }
    
}