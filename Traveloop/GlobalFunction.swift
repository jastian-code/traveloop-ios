//
//  GlobalFunction.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/7/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class GlobalFunction{
    static func getLanguageDict() -> [String:String]{
        
        var languageDict: [String:String] = [:]
        if let selectedLanguage = NSUserDefaults.standardUserDefaults().objectForKey(kSelectedLanguageUserDefault){
            
            switch selectedLanguage {
            case Language.EN : languageDict = kLanguageDictEn; break;
            case Language.ID : languageDict = kLanguageDictID; break;
            default: languageDict = kLanguageDictDefault; break;
            }
            
        }
        
        return languageDict
    }
}