//
//  XmlRequestParameterAirDetailsUpdate.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 1/4/16.
//  Copyright © 2016 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class XmlRequestParameterAirDetailsUpdate : XmlRequestParameterBase {
    var adultPassengerCount: Int = 0
    var childPassengerCount: Int = 0
    var infantPassengerCount: Int = 0
    var originAirportCode: String = ""
    var originCity: String = ""
    var originDisplay: String = "" //(CGK) Jakarta
    var destinationAirportCode: String = ""
    var destinationCity : String = ""
    var destinationDisplay: String = ""
    var isRoundTrip: Bool = false{
        didSet{
            self.xmlFileName = isRoundTrip ? "AirDetailsUpdateXmlRequestRoundTrip" : "AirDetailsUpdateXmlRequest"
        }
    }
    var departureDate : NSDate?
    var departureDateString: String = ""
    var departureDateDisplay: String = ""
    var returnDate: NSDate?
    var returnDateString: String = ""
    var returnDateDisplay: String = ""
    var responseType : APIResponseType = APIResponseType.json{
        didSet{
            switch self.responseType  {
            case APIResponseType.json: self.apiURI = TraveloopAPIUri.flightSearch;break;
            case APIResponseType.xml: self.apiURI = TraveloopAPIUri.flightSearch + ".xml";break;
            }
        }
    }
    
    var flight: FlightSearchResult?
    var flightReturn: FlightSearchResult?
    var airSearchFlightDetailXmlResponse: String = ""
    var airSearchReturnFlightDetailXmlResponse: String = ""
    
    init(){
        super.init(type: APIServiceType.AirDetailsUpdate, HTTPMethod: HTTPMethodType.POST)
        
        self.apiURI = TraveloopAPIUri.flightSearch
        self.xmlFileName = "AirDetailsUpdateXmlRequest"
        
        self.headerParam = ["B2B-Action" : "AirDetailsUpdate"]
    }
    
    override func replaceXmlValue(var xml: NSString)-> NSString{
        xml = xml.stringByReplacingOccurrencesOfString("{AdultPassenger}", withString: "\(self.adultPassengerCount)")
        xml = xml.stringByReplacingOccurrencesOfString("{ChildPassenger}", withString: "\(self.childPassengerCount)")
        xml = xml.stringByReplacingOccurrencesOfString("{InfantPassenger}", withString: "\(self.infantPassengerCount)")
        xml = xml.stringByReplacingOccurrencesOfString("{OriginCityCode}", withString: self.originCity)
        xml = xml.stringByReplacingOccurrencesOfString("{OriginAirportCode}", withString: self.originAirportCode)
        
        xml = xml.stringByReplacingOccurrencesOfString("{DepartureDate}", withString: self.departureDateString)
        xml = xml.stringByReplacingOccurrencesOfString("{ReturnDate}", withString: self.returnDateString)
   
        xml = xml.stringByReplacingOccurrencesOfString("{DestinationAirportCode}", withString: self.destinationAirportCode)
        xml = xml.stringByReplacingOccurrencesOfString("{DestinationCityCode}", withString: self.destinationCity)
        xml = xml.stringByReplacingOccurrencesOfString("{RoundTrip}", withString: self.isRoundTrip ? "Round Trip" : "One Way")
        
        xml = xml.stringByReplacingOccurrencesOfString("{FlightResult}", withString: self.airSearchFlightDetailXmlResponse)
        xml = xml.stringByReplacingOccurrencesOfString("{FlightResultReturn}", withString: self.airSearchReturnFlightDetailXmlResponse)
        
        return xml
    }
    
}