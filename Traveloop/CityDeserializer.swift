//
//  CityDeserializer.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

class CityDeserializer : CoreDataDeserializable{
    
    var apiUrl = TraveloopAPIUri.getCities
    
    func deserialize(context:NSManagedObjectContext, json : JSON) -> [NSManagedObject]{
        var cities = [City]()
        let jsonResult = json["Cities"]
        var isCreate : Bool
        
        for(_, subJson) : (String, JSON) in jsonResult{
            
            let id = subJson["id"].intValue
            
            var city = City.findById(context, id: id)
            if city == nil{
                city = City.create(context)
                city!.id = id
                isCreate = true
            }else{
                isCreate = false
            }
            
            city!.cityName = subJson["cityName"].stringValue
            city!.isActive = subJson["isActive"].boolValue
            
            if let createdDate = subJson["createdDate"].stringValue as? String{
                city!.createdDate = DateUtility.dateFromString(createdDate, format: "yyyy/MM/dd")
            }
            if let modifiedDate = subJson["modifiedDate"].stringValue as? String{
                city!.modifiedDate = DateUtility.dateFromString(modifiedDate, format: "yyyy/MM/dd")
            }
            
            if let fk = subJson["FKCountryId"].intValue as? Int{
                if let country = Country.findById(context, id: fk){
                    city!.country = country
                    
                    let cities = city!.country?.mutableOrderedSetValueForKey("cities")
                    
                    if isCreate{
                        cities?.addObject(city!)
                    }
                    
                }
                
            }
            
            cities.append(city!)
        }
        
        return cities
        
    }
}