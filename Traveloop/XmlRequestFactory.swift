//
//  XmlRequestHelper.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/23/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class XmlRequestFactory{
    var params: XmlRequestParameter
    init(params: XmlRequestParameter){
        self.params = params
    }
    
    private func addHeaderParam(request: NSMutableURLRequest){
        request.setValue("application/xml", forHTTPHeaderField: "Content-Type")

        for (key, value) in self.params.headerParam{
            request.setValue(value, forHTTPHeaderField: key)

        }
    }
    
    
    private func createXml() -> NSData{
        let location = NSBundle.mainBundle().pathForResource(params.xmlFileName, ofType: "xml")
        let xmlString = try? NSString(contentsOfFile: location!, encoding: NSUTF8StringEncoding)
        let xmlStringReplaced = self.params.replaceXmlValue(xmlString!)
        
        let xmlData = xmlStringReplaced.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
        
        return xmlData
    }
    
    private func createRequest() -> NSMutableURLRequest{
        let request = NSMutableURLRequest(URL: NSURL(string: params.apiURI)!)
        request.HTTPMethod = params.HTTPMethod.rawValue
        
        return request
        
    }
    
    func generate() -> NSMutableURLRequest{
        let xmlData = createXml()
        let request = createRequest()
        request.HTTPBody = xmlData
        addHeaderParam(request);
        
        return request
    }
}