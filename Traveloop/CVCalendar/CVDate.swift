//
//  CVDate.swift
//  CVCalendar
//
//  Created by Мак-ПК on 12/31/14.
//  Copyright (c) 2014 GameApp. All rights reserved.
//

import UIKit

public final class CVDate: NSObject {
    private let date: NSDate
    
    public let year: Int
    public let month: Int
    public let week: Int
    public let day: Int
    
   public init(date: NSDate) {
        let dateRange = Manager.dateRange(date)
        
        self.date = date
        self.year = dateRange.year
        self.month = dateRange.month
        self.week = dateRange.weekOfMonth
        self.day = dateRange.day
        
        super.init()
    }
    
    public init(day: Int, month: Int, week: Int, year: Int) {
        if let date = Manager.dateFromYear(year, month: month, week: week, day: day) {
            self.date = date
        } else {
            self.date = NSDate()
        }
        
        self.year = year
        self.month = month
        self.week = week
        self.day = day
        
        super.init()
    }
}

extension CVDate {
    public func convertedDate() -> NSDate? {
        let calendar = NSCalendar.currentCalendar()
        let comps = Manager.componentsForDate(NSDate())
        
        comps.year = year
        comps.month = month
        comps.weekOfMonth = week
        comps.day = day
        
        return calendar.dateFromComponents(comps)
    }
    
    public func getDayOfWeek() -> CVCalendarWeekday{
        let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let weekday = calendar.components(.Weekday, fromDate: date).weekday
        switch weekday{
            case 1 : return CVCalendarWeekday.Sunday
            case 2 : return CVCalendarWeekday.Monday
            case 3 : return CVCalendarWeekday.Tuesday
            case 4 : return CVCalendarWeekday.Wednesday
            case 5 : return CVCalendarWeekday.Thursday
            case 6 : return CVCalendarWeekday.Friday
            case 7 : return CVCalendarWeekday.Saturday
            default: return CVCalendarWeekday.Sunday
        }
    }
    
    public func isSunday() -> Bool{
        let calendar = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian)!
        let weekday = calendar.components(.Weekday, fromDate: date).weekday
        return weekday == 1
    }
}

extension CVDate {
    public var globalDescription: String {
        get {
            let month = dateFormattedStringWithFormat("MMMM", fromDate: date)
            return "\(month), \(year)"
        }
    }
    
    public var commonDescription: String {
        get {
            let month = dateFormattedStringWithFormat("MMMM", fromDate: date)
            return "\(day) \(month), \(year)"
        }
    }
    
    public var customDescriptionValue: String{
        get{
            let month = dateFormattedStringWithFormat("MM", fromDate: date)
            return "\(year)-\(month)-\(day)"
        }
    }
    public var customDescriptionText: String{
        get{
            let month = dateFormattedStringWithFormat("MMM", fromDate: date)
            let dayName = dateFormattedStringWithFormat("EEEE", fromDate: date)
            return "\(dayName), \(day) \(month) \(year)"
        }
    }
}

private extension CVDate {
    func dateFormattedStringWithFormat(format: String, fromDate date: NSDate) -> String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = format
        return formatter.stringFromDate(date)
    }
}
