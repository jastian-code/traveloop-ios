//
//  MenuViewController.swift
//  Traveloop
//
//  Created by Apple on 9/28/15.
//  Copyright (c) 2015 PT. Code Development Indonesia. All rights reserved.
//

import UIKit

class MenuViewController: UIViewController {
    
    let cellIdentifiers: [String] = ["FindFlight&Hotel", "MyBooking", "E-Ticket", "LogIn", "Help"]

    @IBOutlet weak var subViewSideMenu: UIView!
    @IBOutlet weak var imageUser: UIImageView!
    
    var languageDict : [String: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        languageDict = GlobalFunction.getLanguageDict()
        
        let backgroundView = UIView(frame: subViewSideMenu.frame)
        
//        backgroundView.backgroundColor = UIColor(patternImage: UIImage(named: "sidebarbackground.jpg")!)
//        backgroundView.contentMode = .ScaleAspectFit
//        backgroundView.alpha = 0.5
//        subViewSideMenu.addSubview(backgroundView)
        
        imageUser.frame.size.height = imageUser.frame.size.width
        imageUser.layer.cornerRadius = 45
        imageUser.layer.borderColor = UIColor(red: 17/255, green: 135/255, blue: 176/255, alpha: 1.0).CGColor
        imageUser.layer.borderWidth = 4.0
        imageUser.clipsToBounds = true

        // Do any additional setup after loading the view.
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}

extension MenuViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.cellIdentifiers.count;
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(self.cellIdentifiers[indexPath.row], forIndexPath: indexPath) as? UITableViewCell
        
        if let label = cell?.viewWithTag(indexPath.row + 1) as? UILabel{
            
            label.text = languageDict[cellIdentifiers[indexPath.row]]!
            
            if(indexPath.row == 3){
                //todo: if Aunthenticated text change to Account
                let isAuthenticated = false
                if isAuthenticated{
                    label.text = languageDict["Account"]!
                }
            }
        }
        
        return cell!
    }
}

extension MenuViewController: UITableViewDelegate {
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedColor = UIColor(red: 17/255, green: 135/255, blue: 176/255, alpha: 1.0)
        let selectedCell = tableView.cellForRowAtIndexPath(indexPath)
        selectedCell?.contentView.backgroundColor = selectedColor
    }
    
    func tableView(tableView: UITableView, didDeselectRowAtIndexPath indexPath: NSIndexPath) {
        let defaultColor = UIColor(red: 19/255, green: 156/255, blue: 235/255, alpha: 1.0)
        let cellToDeselect = tableView.cellForRowAtIndexPath(indexPath)
        cellToDeselect?.contentView.backgroundColor = defaultColor
    }
}
