//
//  ContextManager.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/14/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation
import CoreData

class ContextManager{
    static let sharedInstance = ContextManager()
    private var appDelegate: AppDelegate
    private var context : NSManagedObjectContext
    
    init(){
        appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        context = appDelegate.managedObjectContext!
    }
    
    class func getContext() -> NSManagedObjectContext{
        return sharedInstance.context
    }
}