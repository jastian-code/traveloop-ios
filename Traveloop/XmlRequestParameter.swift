//
//  XmlRequestParameter.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 11/25/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

protocol XmlRequestParameter{
    var apiServiceType : APIServiceType{ get set }
    var HTTPMethod : HTTPMethodType { get set }
    var xmlFileName : String { get set}
    
    var apiURI: String {get set}
    
    var headerParam : [String:String] {get set}
    
    func replaceXmlValue(var xml: NSString)-> NSString
}