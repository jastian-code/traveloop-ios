//
//  Airport+CoreDataProperties.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/14/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Airport {

    @NSManaged var id: NSNumber?
    @NSManaged var airportCode: String?
    @NSManaged var airportName: String?
    @NSManaged var isActive: NSNumber?
    @NSManaged var createdDate: NSDate?
    @NSManaged var modifiedDate: NSDate?
    @NSManaged var city: City?

    var nameIdOption : String{
        
        return "(\(id!)) \(airportName!)"
    }
    var nameCodeOption:String{
        return "(\(airportCode!)) \(airportName!)"
    }
    var cityCountryOption:String{
        var cityName = ""
        var countryName = ""
        
        if city != nil{
            cityName = city!.cityName!
            if city!.country != nil{
                countryName = city!.country!.countryName!
            }
        }
        return "\(cityName), \(countryName)"
        
    }
    var cityIdOption:String{
        var cityName = ""
        
        if city != nil{
            cityName = city!.cityName!
        }
        
        return "(\(id!)) \(cityName)"
    }
    var codeCityOption:String{
        var cityName = ""
        
        if city != nil{
            cityName = city!.cityName!
        }
        
        return "(\(airportCode!)) \(cityName)"
    }
    var countryId: NSNumber?{
        
        if city != nil{
            if city!.country != nil{
                return city!.country!.id
            }
        }
        return 0
        
    }
    var countryName: String {
        var country = ""
        
        if city != nil{
            if city!.country != nil{
                country = city!.country!.countryName!
            }
        }
        return "\(country)"
    }
    var nameCodeCityCountryOption:String{
        var cityName = ""
        var countryName = ""
        
        if city != nil{
            cityName = city!.cityName!
            if city!.country != nil{
                countryName = city!.country!.countryName!
            }
        }
        
        return "(\(airportCode!)) \(airportName!) \(cityName), \(countryName)"
    }
}
