//
//  DateUtility.swift
//  Traveloop
//
//  Created by CODE-MAC1 on 12/16/15.
//  Copyright © 2015 PT. Code Development Indonesia. All rights reserved.
//

import Foundation

class DateUtility{
    static func dateFromString(dateString: String, format: String) -> NSDate?{
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.dateFromString(dateString)
        return date
    }
    
    static func stringFromDate(date: NSDate, format: String) -> String{
        var dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.stringFromDate(date)
    }
    
}